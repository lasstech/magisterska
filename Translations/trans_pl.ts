<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl_PL">
<context>
    <name>CustomisedWidget</name>
    <message>
        <source>Axial</source>
        <translation type="vanished">Odgórnie</translation>
    </message>
    <message>
        <source>Sagittal</source>
        <translation type="vanished">Profilowo</translation>
    </message>
    <message>
        <source>Coronal</source>
        <translation type="vanished">Frontalnie</translation>
    </message>
</context>
<context>
    <name>DialogLogin</name>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_dialoglogin.h" line="168"/>
        <source>Log in window</source>
        <translation>Okno logowania</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_dialoglogin.h" line="169"/>
        <source>Insert Login and Password</source>
        <translation>Podaj login i hasło</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_dialoglogin.h" line="172"/>
        <source>Log in</source>
        <translation>Logowanie</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_dialoglogin.h" line="173"/>
        <source>Sign in</source>
        <translation>Rejestracja</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_dialoglogin.h" line="175"/>
        <source>Password</source>
        <translation>Hasło</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_dialoglogin.h" line="177"/>
        <source>Cancel</source>
        <translation>Cofnij</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_dialoglogin.h" line="176"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>login allready exists</source>
        <translation type="vanished">Login już istnieje</translation>
    </message>
    <message>
        <source>Sign in window</source>
        <translation type="vanished">Okno rejestracji</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>open file</source>
        <translatorcomment>komenda służąca do otwarcia pliku</translatorcomment>
        <translation type="vanished">Otwórz plik</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="648"/>
        <location filename="../UIComponents/Mainwindow/mainwindow.cpp" line="78"/>
        <source>Open file</source>
        <translatorcomment>komenda służąca do otwarcia pliku</translatorcomment>
        <translation>Otwórz plik</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="649"/>
        <source>Convert to NRRD</source>
        <translation>Zamień na NRRD</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="657"/>
        <source>Morphological operations</source>
        <translation>Operacje morfologiczne</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="659"/>
        <source>Close</source>
        <translation>Domknięcie</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="660"/>
        <source>Dilate</source>
        <translation>Dylatacja</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="661"/>
        <source>Erode</source>
        <translation>Erozja</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="662"/>
        <source>Original</source>
        <translation>Oryginał</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="663"/>
        <source>Reconstruction</source>
        <translation>Rekonstrukcja</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="664"/>
        <source>Range</source>
        <translation>Zasięg</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="666"/>
        <source>Morfological</source>
        <translation>Przekształcenia</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="667"/>
        <source>Painting mode</source>
        <translation>Tryb Malowania</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="670"/>
        <location filename="../UIComponents/Mainwindow/mainwindow.cpp" line="819"/>
        <source>Slices count</source>
        <translation>Zasięg malowania</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="673"/>
        <source>Painting</source>
        <translation>Malowanie</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="679"/>
        <source>View Rotation</source>
        <translation>Rotacja widokami</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="686"/>
        <source>Data Rotation</source>
        <translation>Rotacja danymi</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="693"/>
        <source>Basic panel</source>
        <translation>Panel resetu</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="698"/>
        <source>Language</source>
        <translation>Język</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="653"/>
        <source>Nr</source>
        <translation>Nr</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="647"/>
        <source>RadioSpace</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="650"/>
        <source>Polish</source>
        <translation>polski</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="651"/>
        <source>English</source>
        <translation>angielski</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="654"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="655"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <source>Interaction type</source>
        <translation type="vanished">Rodzaj interakcji</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="681"/>
        <source>Absolute</source>
        <translation>Absolutny</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="682"/>
        <source>Relative</source>
        <translation>Względny</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="683"/>
        <source>Vertical axis:</source>
        <translation>Pionowa oś:</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="684"/>
        <source>Frontal axis:</source>
        <translation>Frontowa oś:</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="685"/>
        <source>Horizotal axis:</source>
        <translation>Oś pozioma:</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="665"/>
        <source>Filter</source>
        <translation>Filtr</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="658"/>
        <source>Open</source>
        <translation>Otwarcie</translation>
    </message>
    <message>
        <source>painting</source>
        <translation type="vanished">maluj</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="668"/>
        <source>Compute</source>
        <translation>Przelicz</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="672"/>
        <source>grow paint</source>
        <translation>Poszerz malowanie</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="671"/>
        <source>Dual</source>
        <translation>Obustronny</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="675"/>
        <source>show crosshair</source>
        <translation>pokaż osie</translation>
    </message>
    <message>
        <source>paint</source>
        <translation type="vanished">maluj</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="680"/>
        <source>Data rotation type</source>
        <translation>Rodzaj obrotu danymi</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="674"/>
        <source>mouse interaction type</source>
        <translation>typ interkcji myszką</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="669"/>
        <source>With brush</source>
        <translation>Manualnie</translation>
    </message>
    <message>
        <source>Normalise</source>
        <translation type="vanished">Normalizuj</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="676"/>
        <source>no crosshair rotation</source>
        <translation>nie obracaj osi</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="677"/>
        <source>crosshair rotation</source>
        <translation>obracaj osią</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="678"/>
        <source>crosshair dual rotation</source>
        <translation>obracaj obiema osiami</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="694"/>
        <source>Reset camera</source>
        <translation>Resetuj ustawienie kamery</translation>
    </message>
    <message>
        <source>Controll</source>
        <translation type="vanished">Kontrola</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="688"/>
        <source>Lower Threshold:</source>
        <translation>Dolny próg:</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="689"/>
        <source>Upper Threshold:</source>
        <translation>Górny próg:</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="690"/>
        <source>Get slider values</source>
        <translation>Pobierz wartości suwaka</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="692"/>
        <location filename="../UIComponents/Mainwindow/mainwindow.cpp" line="612"/>
        <source>Export surface</source>
        <translation>Eksportuj przestrzeń</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="691"/>
        <source>Grow region</source>
        <translation>Wyodrębnij kontur</translation>
    </message>
    <message>
        <source>Surface</source>
        <translation type="vanished">Powierzchnia</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="695"/>
        <source>Reload</source>
        <translation>Wczytaj ponownie</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="687"/>
        <source>Surface Panel</source>
        <translation>Panel ekstrakcji powierzchni</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="696"/>
        <source>File</source>
        <translation>Plik</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="697"/>
        <source>Options</source>
        <translation>Opcje</translation>
    </message>
    <message>
        <source>Open File NRRD</source>
        <translation type="vanished">Otwórz plik</translation>
    </message>
    <message>
        <location filename="../UIComponents/Mainwindow/mainwindow.cpp" line="79"/>
        <source>NRRD Files (*.nrrd );;VTK Files (*.vtk );;Dicom Files(*);;STL Files (*.stl )</source>
        <translation>NRRD Pliki (*.nrrd );;VTK Pliki (*.vtk );;Dicom Pliki(*);;STL Pliki (*.stl )</translation>
    </message>
    <message>
        <source>Open NRRD Folder</source>
        <translation type="vanished">Otwórz folder</translation>
    </message>
    <message>
        <location filename="../UIComponents/Mainwindow/mainwindow.cpp" line="111"/>
        <source>Open Folder</source>
        <translation>Otwórz folder</translation>
    </message>
    <message>
        <location filename="../UIComponents/Mainwindow/mainwindow.cpp" line="274"/>
        <location filename="../UIComponents/Mainwindow/mainwindow.cpp" line="810"/>
        <source>Brush size</source>
        <translation>Rozmiar pędzla</translation>
    </message>
    <message>
        <location filename="../UIComponents/Mainwindow/mainwindow.cpp" line="612"/>
        <source>STL Files (*.stl )</source>
        <translation>STL plik (*.stl )</translation>
    </message>
    <message>
        <location filename="../UIComponents/Mainwindow/mainwindow.cpp" line="625"/>
        <location filename="../UIComponents/Mainwindow/mainwindow.cpp" line="797"/>
        <location filename="../UIComponents/Mainwindow/mainwindow.cpp" line="853"/>
        <location filename="../UIComponents/Mainwindow/mainwindow.cpp" line="878"/>
        <location filename="../UIComponents/Mainwindow/mainwindow.cpp" line="904"/>
        <source>no surface to export</source>
        <translation>Brak powierzchni do eksportowania</translation>
    </message>
    <message>
        <source>Horizontal axis: %1</source>
        <translation type="vanished">Pozioma oś:%1</translation>
    </message>
    <message>
        <source>Frontal axis: %1</source>
        <translation type="vanished">Frontalna oś: %1</translation>
    </message>
    <message>
        <source>Vertical axis: %1</source>
        <translation type="vanished">Oś pionowa: %1</translation>
    </message>
    <message>
        <location filename="../UIComponents/Mainwindow/mainwindow.cpp" line="724"/>
        <source>Error</source>
        <translation>Błąd</translation>
    </message>
    <message>
        <location filename="../UIComponents/Mainwindow/mainwindow.cpp" line="729"/>
        <source>Warning</source>
        <translation>Wskazówka</translation>
    </message>
    <message>
        <location filename="../UIComponents/Mainwindow/mainwindow.cpp" line="337"/>
        <source>You have already looked this file up today as: %1</source>
        <translation>Ten plik był już dzisiaj przeglądany jako %1</translation>
    </message>
    <message>
        <location filename="../UIComponents/Mainwindow/mainwindow.cpp" line="381"/>
        <source>Closing window</source>
        <translation>Zamykanie programu</translation>
    </message>
    <message>
        <source>You have already looked this file up today</source>
        <translation type="vanished">Ten plik był już dzisiaj przeglądany</translation>
    </message>
    <message>
        <location filename="../UIComponents/Mainwindow/mainwindow.cpp" line="382"/>
        <source>Do you want to save settings?
</source>
        <translation>Czy chcesz zachować zmiany?</translation>
    </message>
    <message>
        <location filename="../UIComponents/Mainwindow/mainwindow.cpp" line="384"/>
        <source>Yes</source>
        <translation>Tak</translation>
    </message>
    <message>
        <location filename="../UIComponents/Mainwindow/mainwindow.cpp" line="385"/>
        <source>No</source>
        <translation>Nie</translation>
    </message>
    <message>
        <location filename="../UIComponents/Mainwindow/mainwindow.cpp" line="386"/>
        <source>Cancel</source>
        <translation>Cofnij</translation>
    </message>
    <message>
        <location filename="../UIComponents/Mainwindow/mainwindow.cpp" line="488"/>
        <source>Current Language changed to %1</source>
        <translation>Aktualny język zmieniony na %1</translation>
    </message>
    <message>
        <location filename="../UIComponents/Mainwindow/mainwindow.cpp" line="734"/>
        <source>Notification</source>
        <translation>Powiadomienie</translation>
    </message>
</context>
</TS>
