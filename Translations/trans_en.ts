<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>DialogLogin</name>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_dialoglogin.h" line="168"/>
        <source>Log in window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_dialoglogin.h" line="169"/>
        <source>Insert Login and Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_dialoglogin.h" line="172"/>
        <source>Log in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_dialoglogin.h" line="173"/>
        <source>Sign in</source>
        <translation>Sign in</translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_dialoglogin.h" line="175"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_dialoglogin.h" line="177"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_dialoglogin.h" line="176"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="647"/>
        <source>RadioSpace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="648"/>
        <location filename="../UIComponents/Mainwindow/mainwindow.cpp" line="78"/>
        <source>Open file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="649"/>
        <source>Convert to NRRD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="650"/>
        <source>Polish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="651"/>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="653"/>
        <source>Nr</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="654"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="655"/>
        <source>Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="657"/>
        <source>Morphological operations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="659"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="660"/>
        <source>Dilate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="661"/>
        <source>Erode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="662"/>
        <source>Original</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="663"/>
        <source>Reconstruction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="664"/>
        <source>Range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="666"/>
        <source>Morfological</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="667"/>
        <source>Painting mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="673"/>
        <source>Painting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="679"/>
        <source>View Rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="681"/>
        <source>Absolute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="682"/>
        <source>Relative</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="683"/>
        <source>Vertical axis:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="684"/>
        <source>Frontal axis:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="685"/>
        <source>Horizotal axis:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="693"/>
        <source>Basic panel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="665"/>
        <source>Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="658"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="668"/>
        <source>Compute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="671"/>
        <source>Dual</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="687"/>
        <source>Surface Panel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="675"/>
        <source>show crosshair</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="680"/>
        <source>Data rotation type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="674"/>
        <source>mouse interaction type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="669"/>
        <source>With brush</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="672"/>
        <source>grow paint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="676"/>
        <source>no crosshair rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="677"/>
        <source>crosshair rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="678"/>
        <source>crosshair dual rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="694"/>
        <source>Reset camera</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="686"/>
        <source>Data Rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="688"/>
        <source>Lower Threshold:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="670"/>
        <location filename="../UIComponents/Mainwindow/mainwindow.cpp" line="819"/>
        <source>Slices count</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="689"/>
        <source>Upper Threshold:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="690"/>
        <source>Get slider values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="692"/>
        <location filename="../UIComponents/Mainwindow/mainwindow.cpp" line="612"/>
        <source>Export surface</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="691"/>
        <source>Grow region</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="695"/>
        <source>Reload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="696"/>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="697"/>
        <source>Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../Builds/Magisterka_deb/ui_mainwindow.h" line="698"/>
        <source>Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UIComponents/Mainwindow/mainwindow.cpp" line="79"/>
        <source>NRRD Files (*.nrrd );;VTK Files (*.vtk );;Dicom Files(*);;STL Files (*.stl )</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UIComponents/Mainwindow/mainwindow.cpp" line="111"/>
        <source>Open Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UIComponents/Mainwindow/mainwindow.cpp" line="274"/>
        <location filename="../UIComponents/Mainwindow/mainwindow.cpp" line="810"/>
        <source>Brush size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UIComponents/Mainwindow/mainwindow.cpp" line="337"/>
        <source>You have already looked this file up today as: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UIComponents/Mainwindow/mainwindow.cpp" line="381"/>
        <source>Closing window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UIComponents/Mainwindow/mainwindow.cpp" line="382"/>
        <source>Do you want to save settings?
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UIComponents/Mainwindow/mainwindow.cpp" line="384"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UIComponents/Mainwindow/mainwindow.cpp" line="385"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UIComponents/Mainwindow/mainwindow.cpp" line="386"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UIComponents/Mainwindow/mainwindow.cpp" line="488"/>
        <source>Current Language changed to %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UIComponents/Mainwindow/mainwindow.cpp" line="612"/>
        <source>STL Files (*.stl )</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UIComponents/Mainwindow/mainwindow.cpp" line="625"/>
        <location filename="../UIComponents/Mainwindow/mainwindow.cpp" line="797"/>
        <location filename="../UIComponents/Mainwindow/mainwindow.cpp" line="853"/>
        <location filename="../UIComponents/Mainwindow/mainwindow.cpp" line="878"/>
        <location filename="../UIComponents/Mainwindow/mainwindow.cpp" line="904"/>
        <source>no surface to export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UIComponents/Mainwindow/mainwindow.cpp" line="724"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UIComponents/Mainwindow/mainwindow.cpp" line="729"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UIComponents/Mainwindow/mainwindow.cpp" line="734"/>
        <source>Notification</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
