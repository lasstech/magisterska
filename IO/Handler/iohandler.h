#ifndef IOHANDLER_H
#define IOHANDLER_H
#include <QtCore>
#include <mitkStandaloneDataStorage.h>
#include <mitkSurface.h>
#include "IO/SQL/sql.h"

class IOHandler
{
public:
    QString current_user;
    IOHandler();
    ~IOHandler();
    QString openDicom(QString input);
    void clean();
    void resetDataStorage();
    mitk::StandaloneDataStorage::Pointer getDataStorage();
    mitk::TimeGeometry::Pointer getTimeGeometry();
    mitk::StandaloneDataStorage::SetOfObjects::Pointer LoadDataNodes(char *  path);
    void exportSurface(mitk::Surface::Pointer surface, QString fileNameDCM );
    void readSettings();
    void writeSettings();
    QSize getSize();
    QString getLunguage();
    QPoint getPosition();
    bool fileExists(QString path);

    SQL * sql;
    void setSize(QSize size);
    void setLunguage(QString lunguage);
    void setPosition(QPoint pos);
private:
    QStringList nameslist;
    void raportError(QString txt);
    mitk::StandaloneDataStorage::Pointer m_DataStorage;
    QString s_lunguage;
    QSize s_size;
    QPoint s_pos;

};

#endif // IOHANDLER_H
