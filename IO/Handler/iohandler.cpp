#include "iohandler.h"
#include "mitkDicomSeriesReader.h"
#include "mitkIOUtil.h"
#include <QFileInfo>



//using namespace IOHandler;

IOHandler::IOHandler():
sql(new SQL(fileExists("MagisDB1.db"),"MagisDB1.db"))
{
    if (QDir("TempNRRD").exists());
    else {
        QDir().mkdir("TempNRRD");
    }
}


bool IOHandler::fileExists(QString path) {
    QFileInfo check_file(path);
    return check_file.exists() && check_file.isFile();
}


QString IOHandler::openDicom(QString input){
    QString path = QString("/TempNRRD/out%1.nrrd").arg(nameslist.length());
    if(nameslist.contains(input)){
        raportError(QString("folder allready loaded as %1").arg(nameslist.indexOf(input)));
        return QString("error");
    }
    else{
        QProcess *process = new QProcess();
        QString out =QDir::currentPath()+ path;
        QString file =QDir::currentPath()+"/IOConstr -i "+input + " -o "+out;
        process->start(file);
        process->waitForFinished();
        QString output(process->readAllStandardOutput());
        if (process->atEnd()){
            nameslist.append(input);
            return path;
        }
        else{
            raportError(QString(process->readAllStandardOutput()));
            return QString("error");
        }
    }
}

void IOHandler::raportError(QString txt){

    qDebug()<<txt;
}

IOHandler::~IOHandler(){

}
void IOHandler::clean(){
    QDir("TempNRRD").removeRecursively();
    sql->destroy();
    delete this;
}

void IOHandler::resetDataStorage(){
    m_DataStorage = mitk::StandaloneDataStorage::New();
}

mitk::StandaloneDataStorage::Pointer IOHandler::getDataStorage(){
    return m_DataStorage;
}

mitk::TimeGeometry::Pointer IOHandler::getTimeGeometry(){
    return m_DataStorage->ComputeBoundingGeometry3D(m_DataStorage->GetAll());
}

mitk::StandaloneDataStorage::SetOfObjects::Pointer IOHandler::LoadDataNodes(char *  path){




    return mitk::IOUtil::Load(path, *m_DataStorage);
}


void IOHandler::exportSurface(mitk::Surface::Pointer surface, QString fileNameDCM ){
    mitk::IOUtil::Save(surface,fileNameDCM.toUtf8().constData());
}

void IOHandler::writeSettings()
{
    QSettings settings("Politechnika Gdańska", QString("Praca magisterska/%1").arg(current_user).toUtf8().constData());

    settings.beginGroup("MainWindow");
    settings.setValue("size", s_size);
    settings.setValue("pos", s_pos);
    settings.setValue("lunguage", s_lunguage);
    settings.endGroup();

//    settings.beginGroup("MainWindow");
//    settings.setValue("size", window->size());
//    settings.setValue("pos", window->pos());
//    settings.setValue("lunguage", window->m_currLang);
//    settings.endGroup();
}
void IOHandler::readSettings()
{
    QSettings settings("Politechnika Gdańska", QString("Praca magisterska/%1").arg(current_user).toUtf8().constData());

    settings.beginGroup("MainWindow");
    s_size = settings.value("size", QSize(400, 400)).toSize();
    s_pos = settings.value("pos", QPoint(200, 200)).toPoint();
    s_lunguage = settings.value("lunguage").toString();
    settings.endGroup();

}

QSize IOHandler::getSize(){
    return s_size;
}
QString IOHandler::getLunguage(){
    return s_lunguage;
}
QPoint IOHandler::getPosition(){
    return s_pos;
}

void IOHandler::setSize(QSize size){
    s_size = size;
}
void IOHandler::setLunguage(QString lunguage){
    s_lunguage = lunguage;
}
void IOHandler::setPosition(QPoint pos){
    s_pos = pos;
}
