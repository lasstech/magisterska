#ifndef XMLIO_H
#define XMLIO_H

#include <QtXml>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QtCore>
#include <QStandardItem>

class XMLIO : public QStandardItemModel {
    Q_OBJECT
public:
    XMLIO(QStandardItemModel *parent = 0);
    ~XMLIO();
    enum Roles {LineStartRole = Qt::UserRole + 1,    LineEndRole};
    void ReadXMLFile();
    void SaveXMLFile();
    void writeXML(QString Date, QString Type, QString Number, QString Path);
    void retrievElements(QDomElement root, QString tag, QString att);
    void open(QString fileName);
    QDomElement root;
private:
    QXmlStreamReader * reader;
    QXmlStreamWriter * writter;
    QXmlStreamReader m_streamReader;
    QStandardItem* m_currentItem;
};

#endif // XMLIO_H



//class XmlTreeModel : public QStandardItemModel {
//Q_OBJECT
//public:
//enum Roles {LineStartRole = Qt::UserRole + 1,
//LineEndRole};
//explicit XmlTreeModel(QObject *parent = 0);
//public slots:
//void open(QString fileName);
