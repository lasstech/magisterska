#include "xmlio.h"
#include <qmessagebox.h>
#include <QObject>

XMLIO::XMLIO (QStandardItemModel *parent) : QStandardItemModel(parent) {
    reader = new QXmlStreamReader();
    writter = new QXmlStreamWriter();
    QDomDocument document;
    root = document.createElement("data");
}

XMLIO::~XMLIO(){

}
void XMLIO::ReadXMLFile(){

}


void XMLIO::open(QString fileName) {
    QFile file (fileName);
    if (!file.open(QIODevice::ReadOnly)) {
        qDebug() << "Nie mogę otworzyć pliku: " << fileName;
        abort();
    }
    m_streamReader.setDevice(&file);
    while (!m_streamReader.atEnd()) {
        QXmlStreamReader::TokenType tt = m_streamReader.readNext();
        switch (tt) {
        case QXmlStreamReader::StartElement: {
            QString name = m_streamReader.name().toString();
            QStandardItem* item = new QStandardItem(name);
            item->setData(m_streamReader.lineNumber(),
                          LineStartRole);
            QXmlStreamAttributes attrs = m_streamReader.attributes();
            QStringList sl;
            sl << tr("Line# %1").arg(m_streamReader.lineNumber());
            foreach (QXmlStreamAttribute attr, attrs) {
                QString line = QString("%1='%2'").arg(attr.name().toString())
                        .arg(attr.value().toString());
                sl.append(line);
            }
            item->setToolTip(sl.join("\n"));
            if (m_currentItem == 0)
                setItem(0, 0, item);
            else
                m_currentItem->appendRow(item);
            m_currentItem = item;
            break; }
        case QXmlStreamReader::Characters: {
            QString tt = m_currentItem->toolTip();
            tt.append("\n");
            tt.append(m_streamReader.text().toString());
            m_currentItem->setToolTip(tt);
            break; }
        case QXmlStreamReader::EndElement:
            m_currentItem->setData(m_streamReader.lineNumber(), LineEndRole);
            m_currentItem = m_currentItem->parent();
            break;
        case QXmlStreamReader::EndDocument:
        default:
            break;
        }
    }
}
void XMLIO::retrievElements(QDomElement root, QString tag, QString att)
{
    QFile file("output.xml");
    QDomNodeList nodes = root.elementsByTagName(tag);

    qDebug() << "# nodes = " << nodes.count();
    for(int i = 0; i < nodes.count(); i++)
    {
        QDomNode elm = nodes.at(i);
        if(elm.isElement())
        {
            QDomElement e = elm.toElement();
            qDebug() << e.attribute(att);
        }
    }
}



void XMLIO::SaveXMLFile(){
    // Create a document to write XML
    QDomDocument document;

    // Making the root element
    root = document.createElement("Dorms");

    // Adding the root element to the docuemnt
    document.appendChild(root);

    // Adding more elements
    for(int i = 0; i < 5; i++)
    {
        QDomElement dorm = document.createElement("Dorm");
        dorm.setAttribute("Name", "Dorm Building " + QString::number(i));
        dorm.setAttribute("ID", QString::number(i));
        root.appendChild(dorm);

        // Adding rooms to each dorm building
        for(int j = 0; j < 3; j++)
        {
            QDomElement room = document.createElement("Room");
            room.setAttribute("Name", "Room " + QString::number(j));
            room.setAttribute("ID", QString::number(j));
            dorm.appendChild(room);
        }
    }

    // Writing to a file
    QFile file("output.xml");
    if(!file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        qDebug() << "Open the file for writing failed";
    }
    else
    {
        QTextStream stream(&file);
        stream << document.toString();
        file.close();
        qDebug() << "Writing is done";
    }

}

//void XMLIO::writeXML(QString Date, QString Type, QString Number, QString Path)
//{
//    QDomDocument doc("mydocument");
//    QFile file("mydocument.xml");
//    if (!file.open(QIODevice::ReadOnly))
//        return;
//    if (!doc.setContent(&file)) {
//        file.close();
//        return;
//    }
//    file.close();

//    // print out the element names of all elements that are direct children
//    // of the outermost element.
//    QDomElement docElem = doc.documentElement();

//    QDomNode n = docElem.firstChild();
//    while(!n.isNull()) {
//        QDomElement e = n.toElement(); // try to convert the node to an element.
//        if(!e.isNull()) {
//            cout << qPrintable(e.tagName()) << endl; // the node really is an element.
//        }
//        n = n.nextSibling();
//    }

//    // Here we append a new element to the end of the document
//    QDomElement elem = doc.createElement("img");
//    elem.setAttribute("src", "myimage.png");
//    docElem.appendChild(elem);
//}
