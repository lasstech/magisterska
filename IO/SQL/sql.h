#ifndef SQL_H
#define SQL_H

#include <QSqlDatabase>
#include <QtSql>

class SQL
{
public:
    SQL(bool ifcreate, const QString &path);
    void destroy();
    bool SQLInsertuser(const QString &name, const QString &password);
    QList<QStringList>  SQLSelectUsers();
    QString SQLSelectUser(int index, const QString &user);
    QList<QStringList>  SQLSelectSession(const QString &user);
    bool SQLInsertSession(const QString &type, const QString &number, const QString &path, const QString &user);
    bool SQLInsert(int Tab, QList<int> enums, QStringList values);
    bool SQLDelete(int Tab, QList<int> enums, QStringList values);
private:
    QSqlDatabase m_db;
    const QStringList Tables={"Users","Session"};
    const QStringList TabUsers={"ID","Name","Pass"};
   const  QStringList TabSession={"ID","User_ID","Date","Type","Number","Path"};
};

#endif // SQL_H

