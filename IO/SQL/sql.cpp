#include "sql.h"
#include "enums.h"

SQL::SQL(bool ifexists, const QString &path)
{
    m_db = QSqlDatabase::addDatabase("QSQLITE");
    m_db.setDatabaseName(path);
    if (ifexists){

        if (!m_db.open())
        {
            qDebug() << "Error: connection with database fail";
        }
        else
        {
            qDebug() << "Database: connection ok";
        }
    } else {
        m_db.open();
        QSqlQuery query;
        query.exec("CREATE TABLE `Session` ("
                   "`ID`	INTEGER PRIMARY KEY AUTOINCREMENT,"
                   "`User_ID`	INTEGER NOT NULL,"
                   "`Date`	TEXT NOT NULL,"
                   "`Type`	TEXT NOT NULL,"
                  " `Number`	INTEGER NOT NULL,"
                   "`Path`	TEXT NOT NULL"
               ");");
        query.exec("CREATE TABLE `Users` ("
                   "`ID`	INTEGER PRIMARY KEY AUTOINCREMENT,"
                   "`Name`	TEXT UNIQUE,"
                   "`Pass`	TEXT"
               ");");
    }
}


void SQL::destroy(){
    m_db.close();
    delete this;
}

bool SQL::SQLInsertuser(const QString &name, const QString &password)
{
    // you should check if args are ok first...
    QSqlQuery query;
    query.prepare(QString("INSERT INTO %1 (%2, %3) VALUES (:name, :password)")
                  .arg(Tables[(int) SqlEnumTable::User])
            .arg(TabUsers[(int) SqlEnumUsers::Name])
            .arg(TabUsers[(int) SqlEnumUsers::Password])
            );
    query.bindValue(":name", name);
    query.bindValue(":password", password);

    if(query.exec())
    {
        return true;
    }
    else
    {
        qDebug()<<query.executedQuery();
        qDebug() << "addPerson error:  "
                 << query.lastError();
    }
    return false;
}


QList<QStringList> SQL::SQLSelectUsers(){
    QString querybuilder =QString("SELECT * FROM %1").arg(Tables[(int) SqlEnumTable::User]);
    QSqlQuery query(querybuilder);
    int id = query.record().indexOf(TabUsers[(int)SqlEnumUsers::ID]);
    int idName = query.record().indexOf(TabUsers[(int)SqlEnumUsers::Name]);
    int pass = query.record().indexOf(TabUsers[(int)SqlEnumUsers::Password]);
    query.exec();
    QList<QStringList>select;
    while (query.next())
    {
        QStringList list = QStringList();
        list.append(query.value(idName).toString());
        list.append(query.value(id).toString());
        list.append(query.value(pass).toString());
        select.append(list);
    }
    return select;
}

QString SQL::SQLSelectUser(int index, const QString &user){

    QString querybuilder =QString("SELECT * FROM %1 where %2 = :user")
            .arg(Tables[(int) SqlEnumTable::User])
            .arg(TabUsers[(int) SqlEnumUsers::Name]);
    QSqlQuery query;
            query.prepare(querybuilder);
            query.bindValue(":user", user);
    query.exec();
    if (query.next())
    {
        int id = query.record().indexOf(TabUsers[index]);
        return query.value(id).toString();
    }
    return "error";
}



QList<QStringList> SQL::SQLSelectSession(const QString &user){

    QString querybuilder =QString("SELECT * FROM %1 where %2 = :user")
            .arg(Tables[(int) SqlEnumTable::Session])
            .arg(TabSession[(int) SqlEnumSession::User]);

    QSqlQuery query;
    query.prepare(querybuilder);
    query.bindValue(":user", SQLSelectUser((int)SqlEnumUsers::ID,user));
    query.exec();
    int date = query.record().indexOf(TabSession[(int)SqlEnumSession::Date]);
    int type = query.record().indexOf(TabSession[(int)SqlEnumSession::Type]);
    int path = query.record().indexOf(TabSession[(int)SqlEnumSession::Path]);
    QList<QStringList>select;
    while (query.next())
    {
        QStringList list = QStringList();
        list.append(query.value(date).toString());
        list.append(query.value(type).toString());
        list.append(query.value(path).toString());
        select.append(list);
    }
    return select;
}




bool SQL::SQLInsertSession(const QString &type, const QString &number, const QString &path, const QString &user)
{
    QStringList values = {type, number, path};
    QList<int> enums = {(int) SqlEnumSession::Type,(int) SqlEnumSession::Number,(int) SqlEnumSession::Path};
    QStringList columns = QStringList();
    for (int var = 0; var < enums.length(); ++var) {
        columns.append(TabSession[(int) enums[var]]);
    }
    columns.append(TabSession[(int) SqlEnumSession::User]);
    values.append(SQLSelectUser((int)SqlEnumUsers::ID,user));
    columns.append(TabSession[(int) SqlEnumSession::Date]);
    values.append(QDate::currentDate().toString("dd/MM/yyyy"));
    QString querybuilder =QString("Insert into %1  (%2) values ('%3')")
            .arg(Tables[(int) SqlEnumTable::Session])
            .arg(columns.join(", "))
            .arg(values.join("', '"));
    QSqlQuery query;
    if(query.exec(querybuilder))
    {
        return true;
    }
    else
    {
        qDebug() << "addPerson error:  "
                 << query.lastError();
    }
    return false;
}

