#ifndef ENUMS_H
#define ENUMS_H



enum class SqlEnumTable
  {
     User = 0,
     Session = 1
  };

enum class SqlEnumUsers
  {
    ID = 0,
    Name = 1,
    Password = 2
  };

enum class SqlEnumSession
  {
    ID = 0,
    User = 1,
    Date = 2,
    Type = 3,
    Number = 4,
    Path = 5
  };




#endif // ENUMS_H
