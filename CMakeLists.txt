cmake_minimum_required(VERSION 3.5)

project(Magisterska)


set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTOUIC ON)



include(includes/packages.cmake)
include(includes/files.cmake)
include(includes/libs.cmake)
include(includes/translation.cmake)

add_executable(${PROJECT_NAME} MACOSX_BUNDLE ${CPP_FILES} ${Cmake_form_hdr} ${QM_FILES})

target_link_libraries(${PROJECT_NAME} MitkCore MitkQtWidgetsExt ${ITK_LIBRARIES} ${QT_LIBS} ${Mylibs})

add_custom_command(TARGET ${PROJECT_NAME} PRE_BUILD COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_SOURCE_DIR}/Resources $<TARGET_FILE_DIR:${PROJECT_NAME}>/Resources)
