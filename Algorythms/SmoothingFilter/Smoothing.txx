/*===================================================================

The Medical Imaging Interaction Toolkit (MITK)

Copyright (c) German Cancer Research Center,
Division of Medical and Biological Informatics.
All rights reserved.

This software is distributed WITHOUT ANY WARRANTY; without
even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE.

See LICENSE.txt or http://www.mitk.org for details.

===================================================================*/
#include "UIComponents/Mainwindow/mainwindow.h"

#include <mitkPointSet.h>
#include <mitkProperties.h>

#include <itkConnectedThresholdImageFilter.h>
#include <itkCurvatureFlowImageFilter.h>

#include <mitkImageAccessByItk.h>
#include <mitkImageCast.h>
#include <mitkLevelWindowProperty.h>
#include <IO/Handler/iohandler.h>

template <typename TPixel, unsigned int VImageDimension>
void Smoothing(itk::Image<TPixel, VImageDimension> *itkImage, MainWindow *mainWindow)
{
    typedef itk::Image<TPixel, VImageDimension> ImageType;

    typedef float InternalPixelType;
    typedef itk::Image<InternalPixelType, VImageDimension> InternalImageType;

    // create itk::CurvatureFlowImageFilter for smoothing and set itkImage as input
    typedef itk::CurvatureFlowImageFilter<ImageType, InternalImageType> CurvatureFlowFilter;
    typename CurvatureFlowFilter::Pointer smoothingFilter = CurvatureFlowFilter::New();

    smoothingFilter->SetInput(itkImage);
    smoothingFilter->SetNumberOfIterations(10);
    smoothingFilter->SetTimeStep(0.0625);

    smoothingFilter->GetOutput()->Update();
    mitk::Image::Pointer mitkImage = mitk::Image::New();
    mitk::CastToMitkImage(smoothingFilter->GetOutput(), mitkImage);

    mainWindow->m_fdataNode->SetData(mitkImage);
    mainWindow->m_FirstImage = static_cast<mitk::Image *>(mainWindow->m_fdataNode->GetData());
}

template <typename TPixel, unsigned int VImageDimension>
void SmoothingR(itk::Image<TPixel, VImageDimension> *itkImage, MainWindow *mainWindow)
{
    typedef itk::Image<TPixel, VImageDimension> ImageType;

    typedef float InternalPixelType;
    typedef itk::Image<InternalPixelType, VImageDimension> InternalImageType;

    // create itk::CurvatureFlowImageFilter for smoothing and set itkImage as input
    typedef itk::CurvatureFlowImageFilter<ImageType, InternalImageType> CurvatureFlowFilter;
    typename CurvatureFlowFilter::Pointer smoothingFilter = CurvatureFlowFilter::New();

    smoothingFilter->SetInput(itkImage);
    smoothingFilter->SetNumberOfIterations(10);
    smoothingFilter->SetTimeStep(0.0625);

    smoothingFilter->GetOutput()->Update();
    mitk::Image::Pointer mitkImage = mitk::Image::New();
    mitk::CastToMitkImage(smoothingFilter->GetOutput(), mitkImage);

    mainWindow->m_ResultNode->SetData(mitkImage);
    mainWindow->m_ResultImage = static_cast<mitk::Image *>(mainWindow->m_ResultNode->GetData());
}

/**
\example Step6RegionGrowing.txx
*/
