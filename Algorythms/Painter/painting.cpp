#include "painting.h"
#include <utility>

#include <mitkImagePixelWriteAccessor.h>
#include <mitkInteractionPositionEvent.h>

Painting::Painting(mitk::Image::Pointer im):image(im)
,geometry(im->GetGeometry())
,indexy(QList<itk::Index<3>>())
,startingIndex(QList<itk::Index<2>>())
,imagetype(image->GetPixelType().GetComponentType())
{
    m_LastPixelIndex.SetElement(0,0);
    m_LastPixelIndex.SetElement(1,0);
    m_LastPixelIndex.SetElement(2,0);
}

Painting::~Painting()
{
    //    delete this;
}

//template <typename T>
//void inline Painter(itk::SmartPointer<mitk::Image> image, itk::Index<3> index, unsigned int timeStep)
//{
//    // As soon as the ImagePixelWriteAccessor object goes out of scope at the
//    // end of this function, the image will be unlocked again (RAII).
//    mitk::ImagePixelWriteAccessor<T> writeAccessor(image, image->GetVolumeData(timeStep));
//    if (writeAccessor.GetPixelByIndex(index)!=std::numeric_limits<T>::min())
//        writeAccessor.SetPixelByIndex(index, std::numeric_limits<T>::min());
//}

void Painting::Paint(itk::Index<3> index)
{
    switch (imagetype)
    {
    case itk::ImageIOBase::CHAR:
        Painter<char>(image, index, geometry->GetTimeStamp());
        break;

    case itk::ImageIOBase::UCHAR:
        Painter<unsigned char>(image, index, geometry->GetTimeStamp());
        break;

    case itk::ImageIOBase::SHORT:
        Painter<short>(image, index, geometry->GetTimeStamp());
        break;

    case itk::ImageIOBase::USHORT:
        Painter<unsigned short>(image, index, geometry->GetTimeStamp());
        break;

    case itk::ImageIOBase::INT:
        Painter<int>(image, index, geometry->GetTimeStamp());
        break;

    case itk::ImageIOBase::UINT:
        Painter<unsigned int>(image, index, geometry->GetTimeStamp());
        break;

    case itk::ImageIOBase::LONG:
        Painter<long>(image, index, geometry->GetTimeStamp());
        break;

    case itk::ImageIOBase::FLOAT:
        Painter<float>(image, index, geometry->GetTimeStamp());
        break;

    case itk::ImageIOBase::DOUBLE:
        Painter<double>(image, index, geometry->GetTimeStamp());
        break;

    default:
        mitkThrow();
    }
}


void Painting::RunPainting(mitk::Point3D position, int brushsize){
    itk::Index<3> index;
    geometry->WorldToIndex<3>(position, index);
    if (index != m_LastPixelIndex)
    {
        for (int var = -brushsize; abs(var) < brushsize+1; ++var) {
            for (int var2 = -brushsize+abs(var); abs(var) + abs(var2) < brushsize+1; ++var2) {
                for (int var3 = -brushsize+abs(var2)+abs(var); abs(var)+abs(var2)+abs(var3) < brushsize+1; ++var3) {
                    itk::Index<3> index2 = itk::Index<3>{index[0]+var,index[1]+var2,index[2]+var3};
                if (geometry->IsIndexInside(index2)){
                    Paint(index2);
                    }
                }
            }
        }
        image->Modified();
        mitk::RenderingManager::GetInstance()->RequestUpdateAll();
    }
}


QList<itk::Index<3>> Painting::GetPath()
{
    return path;
}

void Painting::addStartingIndex(itk::Index<2> index)
{
    startingIndex.append(index);
}

void Painting::justpaint(mitk::Point3D position)
{
    itk::Index<3> index;
    geometry->WorldToIndex<3>(position, index);
    Paint(index);
}

double Painting::getPixel(mitk::Point3D position)
{
    return image->GetPixelValueByWorldCoordinate(position);
}

void Painting::refresh()
{
    image->Modified();
    mitk::RenderingManager::GetInstance()->RequestUpdateAll();
}
