#ifndef PAINTING_H
#define PAINTING_H

#include <mitkImage.h>
#include <QList>
#include <mitkImagePixelWriteAccessor.h>


class Painting
{
public:
    Painting(mitk::Image::Pointer im);
    virtual ~Painting();
    void Paint(itk::Index<3> index);
    void RunPainting(mitk::Point3D position,int brushsize);
//    void RunPathing(itk::Index<3> index);
    void setNewList();
    QList<itk::Index<3>> GetPath();
    void addStartingIndex(itk::Index<2> index);
    void fixpath(QList<itk::Index<2>> *path);
    void justpaint(mitk::Point3D position);
    double getPixel(mitk::Point3D position);
    void refresh();
public:
    template <typename T>
    void inline Painter(itk::SmartPointer<mitk::Image> image, itk::Index<3> index, unsigned int timeStep)
    {
        mitk::ImagePixelWriteAccessor<T> writeAccessor(image, image->GetVolumeData(timeStep));
            writeAccessor.SetPixelByIndex(index, std::numeric_limits<T>::min());
    }
protected:
    bool newlist;
    QList<itk::Index<3>>indexy;
    QList<itk::Index<2>>startingIndex;
    QList<itk::Index<3>>path;
    itk::Index<3> m_LastPixelIndex;
    mitk::BaseGeometry *geometry;
    mitk::Image::Pointer image;
    int imagetype;
};

#endif // PAINTING_H
