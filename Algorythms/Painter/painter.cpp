#include "painter.h"

#include <mitkImage.h>
#include <mitkImagePixelWriteAccessor.h>
#include <mitkInteractionPositionEvent.h>
#include <qdebug.h>

//using namespace Algorythms::Painter;
template <typename T>
void Paint(itk::SmartPointer<mitk::Image> image, itk::Index<3> index, unsigned int timeStep)
{

  // As soon as the ImagePixelWriteAccessor object goes out of scope at the
  // end of this function, the image will be unlocked again (RAII).
  mitk::ImagePixelWriteAccessor<T> writeAccessor(image, image->GetVolumeData(timeStep));
    writeAccessor.GetPixelByIndex(index);

    writeAccessor.SetPixelByIndex(index, std::numeric_limits<T>::min());

  // Don't forget to update the modified time stamp of the image. Otherwise,
  // everything downstream wouldn't recognize that the image changed,
  // including the rendering system.
  image->Modified();
}

void Paint(mitk::Image::Pointer image, itk::Index<3> index, unsigned int timeStep)
{
    int i = image->GetPixelType().GetComponentType();
//    std::cout <<
  switch (i)
  {
  case itk::ImageIOBase::CHAR:
    Paint<char>(image, index, timeStep);
    break;

  case itk::ImageIOBase::UCHAR:
    Paint<unsigned char>(image, index, timeStep);
    break;

  case itk::ImageIOBase::SHORT:
    Paint<short>(image, index, timeStep);
    break;

  case itk::ImageIOBase::USHORT:
    Paint<unsigned short>(image, index, timeStep);
    break;

  case itk::ImageIOBase::INT:
    Paint<int>(image, index, timeStep);
    break;

  case itk::ImageIOBase::UINT:
    Paint<unsigned int>(image, index, timeStep);
    break;

  case itk::ImageIOBase::LONG:
    Paint<long>(image, index, timeStep);
    break;

  case itk::ImageIOBase::FLOAT:
    Paint<float>(image, index, timeStep);
    break;

  case itk::ImageIOBase::DOUBLE:
    Paint<double>(image, index, timeStep);
    break;

  default:
    mitkThrow();
  }
}
