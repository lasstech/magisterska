#include "mitkVector.h"
#include "mitkBaseGeometry.h"

class Vector
{
public:
    Vector();
    virtual ~Vector();
public:
    mitk::Vector3D                  cross();
    double scalar(mitk::Vector3D &a, mitk::Vector3D&b);
    void rotate(mitk::BaseGeometry &geometry, mitk::Vector3D &b, int i);
};
