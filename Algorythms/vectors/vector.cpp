#include "vector.h"
#include <math.h>
#include"mitkRotationOperation.h"
#include <mitkInteractionConst.h>


#define PI 3.14159265

Vector::Vector()
{
}

Vector::~Vector()
{
    //    delete this;
}


double Vector::scalar(mitk::Vector3D& a,mitk::Vector3D& b){
    double licz = a[0]*b[0]+ a[1]*b[1]+ a[2]*b[2];
    double mian = std::sqrt(pow(a[0],2)+pow(a[1],2)+pow(a[2],2))*std::sqrt(pow(b[0],2)+pow(b[1],2)+pow(b[2],2));
    return acos(licz/mian)* 180.0 /PI;
}


void Vector::rotate(mitk::BaseGeometry &geometry, mitk::Vector3D &b, int i){

    mitk::RotationOperation *op;

    mitk::Vector3D vect = geometry.GetAxisVector(i);
    mitk::Vector3D Planevect = itk::CrossProduct(b,vect);
    double an = scalar(b,vect);
    op = new mitk::RotationOperation(mitk::OpROTATE, geometry.GetCenter(), Planevect, -an);
    geometry.ExecuteOperation(op);
    op->~Operation();
}
