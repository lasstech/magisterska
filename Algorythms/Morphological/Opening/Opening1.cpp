/*===================================================================

The Medical Imaging Interaction Toolkit (MITK)

Copyright (c) German Cancer Research Center,
Division of Medical and Biological Informatics.
All rights reserved.

This software is distributed WITHOUT ANY WARRANTY; without
even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE.

See LICENSE.txt or http://www.mitk.org for details.

===================================================================*/

#include "Opening.txx"

#include <mitkInstantiateAccessFunctions.h>

#define InstantiateAccessFunction_Opening(pixelType, dim)                                                     \
  \
template void                                                                                                          \
    Opening(itk::Image<pixelType, dim> *, MainWindow *);
InstantiateAccessFunctionForFixedDimension(Opening, 2)

#define InstantiateAccessFunction_OpeningR(pixelType, dim)                                                     \
  \
template void                                                                                                          \
    OpeningR(itk::Image<pixelType, dim> *, MainWindow *);
InstantiateAccessFunctionForFixedDimension(OpeningR, 2)

#define InstantiateAccessFunction_Closing(pixelType, dim)                                                     \
  \
template void                                                                                                          \
    Closing(itk::Image<pixelType, dim> *, MainWindow *);
InstantiateAccessFunctionForFixedDimension(Closing, 2)

#define InstantiateAccessFunction_ClosingR(pixelType, dim)                                                     \
  \
template void                                                                                                          \
    ClosingR(itk::Image<pixelType, dim> *, MainWindow *);
InstantiateAccessFunctionForFixedDimension(ClosingR, 2)

#define InstantiateAccessFunction_Dilate(pixelType, dim)                                                     \
  \
template void                                                                                                          \
    Dilate(itk::Image<pixelType, dim> *, MainWindow *);
InstantiateAccessFunctionForFixedDimension(Dilate, 2)

#define InstantiateAccessFunction_DilateR(pixelType, dim)                                                     \
  \
template void                                                                                                          \
    DilateR(itk::Image<pixelType, dim> *, MainWindow *);
InstantiateAccessFunctionForFixedDimension(DilateR, 2)

#define InstantiateAccessFunction_Erode(pixelType, dim)                                                     \
  \
template void                                                                                                          \
    Erode(itk::Image<pixelType, dim> *, MainWindow *);
InstantiateAccessFunctionForFixedDimension(Erode, 2)

#define InstantiateAccessFunction_ErodeR(pixelType, dim)                                                     \
  \
template void                                                                                                          \
    ErodeR(itk::Image<pixelType, dim> *, MainWindow *);
InstantiateAccessFunctionForFixedDimension(ErodeR, 2)

  /**
  \example Step6RegionGrowing1.cpp
  */
