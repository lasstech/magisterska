/*===================================================================

The Medical Imaging Interaction Toolkit (MITK)

Copyright (c) German Cancer Research Center,
Division of Medical and Biological Informatics.
All rights reserved.

This software is distributed WITHOUT ANY WARRANTY; without
even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE.

See LICENSE.txt or http://www.mitk.org for details.

===================================================================*/
#include "UIComponents/Mainwindow/mainwindow.h"

#include <mitkPointSet.h>
#include <mitkProperties.h>


#include <mitkImageAccessByItk.h>
#include <mitkImageCast.h>



#include "itkImage.h"
#include "itkBinaryMorphologicalOpeningImageFilter.h"
#include "itkBinaryDilateImageFilter.h"
#include "itkImageFileReader.h"
#include "itkBinaryBallStructuringElement.h"
#include "itkSubtractImageFilter.h"
#include "itkGrayscaleDilateImageFilter.h"
#include "itkGrayscaleErodeImageFilter.h"




template <typename TPixel, unsigned int VImageDimension>
typename itk::GrayscaleErodeImageFilter<
typename itk::Image<TPixel, VImageDimension>
, typename itk::Image<TPixel, VImageDimension>
, typename itk::BinaryBallStructuringElement<TPixel, VImageDimension>
>::Pointer erode(int bin, itk::Image<TPixel, VImageDimension> *itkImage){

    typedef itk::Image<TPixel, VImageDimension> ImageType;
    typedef itk::BinaryBallStructuringElement<TPixel, VImageDimension>StructuringElementType;
    StructuringElementType structuringElement;
    structuringElement.SetRadius(bin);
    structuringElement.CreateStructuringElement();


    typedef itk::GrayscaleErodeImageFilter <ImageType, ImageType, StructuringElementType> GrayscaleErodeImageFilterType;
    typename GrayscaleErodeImageFilterType::Pointer openingFilter = GrayscaleErodeImageFilterType::New();
    openingFilter->SetInput(itkImage);
    openingFilter->SetKernel(structuringElement);
    openingFilter->GetOutput()->Update();
    return openingFilter;
}


template <typename TPixel, unsigned int VImageDimension>
typename itk::GrayscaleDilateImageFilter<
typename itk::Image<TPixel, VImageDimension>
, typename itk::Image<TPixel, VImageDimension>
, typename itk::BinaryBallStructuringElement<TPixel, VImageDimension>
>::Pointer dilate(int bin, itk::Image<TPixel, VImageDimension> *itkImage){

    typedef itk::Image<TPixel, VImageDimension> ImageType;
    typedef itk::BinaryBallStructuringElement<TPixel, VImageDimension>StructuringElementType;
    StructuringElementType structuringElement;
    structuringElement.SetRadius(bin);
    structuringElement.CreateStructuringElement();


    typedef itk::GrayscaleDilateImageFilter<ImageType, ImageType, StructuringElementType> GrayscaleDilateImageFilterType;
    typename GrayscaleDilateImageFilterType::Pointer openingFilter = GrayscaleDilateImageFilterType::New();
    openingFilter->SetInput(itkImage);
    openingFilter->SetKernel(structuringElement);
    openingFilter->GetOutput()->Update();
    return openingFilter;
}



template <typename TPixel, unsigned int VImageDimension>
void Opening(itk::Image<TPixel, VImageDimension> *itkImage, MainWindow *mainWindow)
{
    int a = mainWindow->binary_steps;
    mitk::Image::Pointer mitkImage = mitk::Image::New();
    mitk::CastToMitkImage(dilate<TPixel, VImageDimension>(a,erode<TPixel, VImageDimension>(a,itkImage)->GetOutput())->GetOutput(), mitkImage);
    mainWindow->m_fdataNode->SetData(mitkImage);
    mainWindow->m_FirstImage = static_cast<mitk::Image *>(mainWindow->m_fdataNode->GetData());
}

template <typename TPixel, unsigned int VImageDimension>
void OpeningR(itk::Image<TPixel, VImageDimension> *itkImage, MainWindow *mainWindow)
{
    int a = mainWindow->binary_steps;
    mitk::Image::Pointer mitkImage = mitk::Image::New();
    mitk::CastToMitkImage(dilate<TPixel, VImageDimension>(a,erode<TPixel, VImageDimension>(a,itkImage)->GetOutput())->GetOutput(), mitkImage);
    mainWindow->m_ResultNode->SetData(mitkImage);
    mainWindow->m_ResultImage = static_cast<mitk::Image *>(mainWindow->m_ResultNode->GetData());
}


template <typename TPixel, unsigned int VImageDimension>
void Closing(itk::Image<TPixel, VImageDimension> *itkImage, MainWindow *mainWindow)
{
    int a = mainWindow->binary_steps;
    mitk::Image::Pointer mitkImage = mitk::Image::New();
    mitk::CastToMitkImage(erode<TPixel, VImageDimension>(a, dilate<TPixel, VImageDimension>(a, itkImage)->GetOutput())->GetOutput(), mitkImage);
    mainWindow->m_fdataNode->SetData(mitkImage);
    mainWindow->m_FirstImage = static_cast<mitk::Image *>(mainWindow->m_fdataNode->GetData());
}

template <typename TPixel, unsigned int VImageDimension>
void ClosingR(itk::Image<TPixel, VImageDimension> *itkImage, MainWindow *mainWindow)
{
    int a = mainWindow->binary_steps;
    mitk::Image::Pointer mitkImage = mitk::Image::New();
    mitk::CastToMitkImage(erode<TPixel, VImageDimension>(a, dilate<TPixel, VImageDimension>(a, itkImage)->GetOutput())->GetOutput(), mitkImage);
    mainWindow->m_ResultNode->SetData(mitkImage);
    mainWindow->m_ResultImage = static_cast<mitk::Image *>(mainWindow->m_ResultNode->GetData());
}

template <typename TPixel, unsigned int VImageDimension>
void Dilate(itk::Image<TPixel, VImageDimension> *itkImage, MainWindow *mainWindow)
{
    int a = mainWindow->binary_steps;
    mitk::Image::Pointer mitkImage = mitk::Image::New();
    mitk::CastToMitkImage(dilate<TPixel, VImageDimension>(a,itkImage)->GetOutput(), mitkImage);
    mainWindow->m_fdataNode->SetData(mitkImage);
    mainWindow->m_FirstImage = static_cast<mitk::Image *>(mainWindow->m_fdataNode->GetData());
}

template <typename TPixel, unsigned int VImageDimension>
void DilateR(itk::Image<TPixel, VImageDimension> *itkImage, MainWindow *mainWindow)
{
    int a = mainWindow->binary_steps;
    mitk::Image::Pointer mitkImage = mitk::Image::New();
    mitk::CastToMitkImage(dilate<TPixel, VImageDimension>(a,itkImage)->GetOutput(), mitkImage);
    mainWindow->m_ResultNode->SetData(mitkImage);
    mainWindow->m_ResultImage = static_cast<mitk::Image *>(mainWindow->m_ResultNode->GetData());
}


template <typename TPixel, unsigned int VImageDimension>
void Erode(itk::Image<TPixel, VImageDimension> *itkImage, MainWindow *mainWindow)
{
    int a = mainWindow->binary_steps;
    mitk::Image::Pointer mitkImage = mitk::Image::New();
    mitk::CastToMitkImage(erode<TPixel, VImageDimension>(a,itkImage)->GetOutput(), mitkImage);
    mainWindow->m_fdataNode->SetData(mitkImage);
    mainWindow->m_FirstImage = static_cast<mitk::Image *>(mainWindow->m_fdataNode->GetData());
}

template <typename TPixel, unsigned int VImageDimension>
void ErodeR(itk::Image<TPixel, VImageDimension> *itkImage, MainWindow *mainWindow)
{
    int a = mainWindow->binary_steps;
    mitk::Image::Pointer mitkImage = mitk::Image::New();
    mitk::CastToMitkImage(erode<TPixel, VImageDimension>(a,itkImage)->GetOutput(), mitkImage);
    mainWindow->m_ResultNode->SetData(mitkImage);
    mainWindow->m_ResultImage = static_cast<mitk::Image *>(mainWindow->m_ResultNode->GetData());
}


/**
\example Step6RegionGrowing.txx
*/
