/*===================================================================

The Medical Imaging Interaction Toolkit (MITK)

Copyright (c) German Cancer Research Center,
Division of Medical and Biological Informatics.
All rights reserved.

This software is distributed WITHOUT ANY WARRANTY; without
even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE.

See LICENSE.txt or http://www.mitk.org for details.

===================================================================*/
#include "UIComponents/Mainwindow/mainwindow.h"

#include <mitkPointSet.h>
#include <mitkProperties.h>

#include <mitkImageAccessByItk.h>
#include <mitkImageCast.h>



#include "itkImage.h"
#include "itkBinaryMorphologicalOpeningImageFilter.h"
#include "itkBinaryDilateImageFilter.h"
#include "itkImageFileReader.h"
#include "itkBinaryBallStructuringElement.h"
#include "itkSubtractImageFilter.h"
#include "itkGrayscaleDilateImageFilter.h"
#include "itkGrayscaleErodeImageFilter.h"






template <typename TPixel, unsigned int VImageDimension>
void OpeningR(itk::Image<TPixel, VImageDimension> *itkImage, MainWindow *mainWindow)
{
    typedef itk::Image<TPixel, VImageDimension> ImageType;


    typedef itk::BinaryBallStructuringElement<TPixel, VImageDimension>StructuringElementType;
    StructuringElementType structuringElement;
    structuringElement.SetRadius(mainWindow->binary_steps);
    structuringElement.CreateStructuringElement();


    typedef itk::GrayscaleErodeImageFilter <ImageType, ImageType, StructuringElementType> GrayscaleErodeImageFilterType;
    typename GrayscaleErodeImageFilterType::Pointer openingFilter = GrayscaleErodeImageFilterType::New();
    openingFilter->SetInput(itkImage);
    openingFilter->SetKernel(structuringElement);
    openingFilter->GetOutput()->Update();


    typedef itk::GrayscaleDilateImageFilter<ImageType, ImageType, StructuringElementType> GrayscaleDilateImageFilterType;
    typename GrayscaleDilateImageFilterType::Pointer openingFilter2 = GrayscaleDilateImageFilterType::New();
    openingFilter2->SetInput(openingFilter->GetOutput());
    openingFilter2->SetKernel(structuringElement);
    openingFilter2->GetOutput()->Update();
    mitk::Image::Pointer mitkImage = mitk::Image::New();
    mitk::CastToMitkImage(openingFilter2->GetOutput(), mitkImage);

    mainWindow->m_ResultNode->SetData(mitkImage);
    mainWindow->m_ResultImage = static_cast<mitk::Image *>(mainWindow->m_ResultNode->GetData());
}



/**
\example Step6RegionGrowing.txx
*/
