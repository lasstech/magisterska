#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <mitkPointSet.h>
#include <QStandardItemModel>
#include <QTreeWidget>
#include <QStringListModel>
#include <QTranslator>

#include <mitkStandaloneDataStorage.h>
#include <mitkImage.h>
#include <mitkSurface.h>
#include "mitkPointSetDataInteractor.h"

class Painting;
class CustomisedWidget;
class IOHandler;
#define acceptable_angle_error 0.00000001

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    virtual void Initialize();
    void setIOHandler(IOHandler *handler);

    int GetThresholdMin();
    int GetThresholdMax();
    void readSettings();

  protected:
    void InitGUI();
    void Load(char * path);
    void callError(QString error);
    void callWarning(QString warning);
    void callInfo(QString warning);
    virtual void SetupNormal();
    void SetAxisLabelH(int position);
    void SetAxisLabelV(int position);
    void SetAxisLabelF(int position);
    bool GetTreeDate(QString date, QString type, QString Path);
    bool GetTreeType(QTreeWidgetItem *parent, QString type, QString Path);
    bool GetTreeChild(QTreeWidgetItem *parent, QString path);
    void changeEvent(QEvent*);
    void CreateLunguages();
    void DataRotator(int number, double position);
    void switchTranslator(QTranslator& translator, const QString& filename);
    void paintIt(mitk::Point3D pos);
    //Algorythms
    template <typename TPixel, unsigned int VImageDimension>
    friend void RegionGrowing(itk::Image<TPixel, VImageDimension> *itkImage, MainWindow *mainWindow);
    template <typename TPixel, unsigned int VImageDimension>
    friend void Smoothing(itk::Image<TPixel, VImageDimension> *itkImage, MainWindow *mainWindow);
    template <typename TPixel, unsigned int VImageDimension>
    friend void SmoothingR(itk::Image<TPixel, VImageDimension> *itkImage, MainWindow *mainWindow);
    template <typename TPixel, unsigned int VImageDimension>
    friend void Opening(itk::Image<TPixel, VImageDimension> *itkImage, MainWindow *mainWindow);
    template <typename TPixel, unsigned int VImageDimension>
    friend void OpeningR(itk::Image<TPixel, VImageDimension> *itkImage, MainWindow *mainWindow);
    template <typename TPixel, unsigned int VImageDimension>
    friend void Closing(itk::Image<TPixel, VImageDimension> *itkImage, MainWindow *mainWindow);
    template <typename TPixel, unsigned int VImageDimension>
    friend void ClosingR(itk::Image<TPixel, VImageDimension> *itkImage, MainWindow *mainWindow);
    template <typename TPixel, unsigned int VImageDimension>
    friend void Dilate(itk::Image<TPixel, VImageDimension> *itkImage, MainWindow *mainWindow);
    template <typename TPixel, unsigned int VImageDimension>
    friend void DilateR(itk::Image<TPixel, VImageDimension> *itkImage, MainWindow *mainWindow);
    template <typename TPixel, unsigned int VImageDimension>
    friend void Erode(itk::Image<TPixel, VImageDimension> *itkImage, MainWindow *mainWindow);
    template <typename TPixel, unsigned int VImageDimension>
    friend void ErodeR(itk::Image<TPixel, VImageDimension> *itkImage, MainWindow *mainWindow);

protected:

    mitk::PointSetDataInteractor::Pointer interactor;
    QList<double>                   *ListOfValues;
    Painting                        *m_painter;
    QList<itk::Index<3>>            *listOfIndex;
    mitk::Image::Pointer            m_FirstImage;
    mitk::DataNode::Pointer         m_fdataNode;
    mitk::PointSet::Pointer         m_Seeds;
    mitk::Image::Pointer            m_ResultImage;
    mitk::DataNode::Pointer         m_ResultNode;
    std::string                     stdstrFolderNameDCM;
    IOHandler                       *iohandler;
    QStringListModel                *model;
    QStringList                     list;
    mitk::TimeGeometry::Pointer     geo;
    QList<mitk::Vector3D>           *RotVectors;
    QList<mitk::Vector3D>           *IMGRVectors;
    QList<double>                   *RotAngles;
    bool                            RotAbs;
    mitk::Vector3D                  RotationVectors[3];
    int                             changed[3];
    int                             RotAxis;
    double                          LastPosition[3];
    bool                            isabsrot; //czy w czasie obrotu ustawiony był ostatnio obrót względny
    mitk::Vector3D                  Initvect[3];
    int                             n_slides;
    int                             s_brush;
    int                             binary_steps;

private slots:
    void slotLanguageChanged(QAction* action);
    void Open();
    void ConvertToNRRD();
    void writeSettings();
    void closeEvent(QCloseEvent *event);

    void on_get_slicer_values_clicked();
    void on_button_grow_clicked();
    void on_button_reload_clicked();
    void on_button_export_surface_clicked();
    void on_axis_slider_v_sliderMoved(int position);
    void on_axis_slider_f_sliderMoved(int position);
    void on_axis_slider_h_sliderMoved(int position);
    void on_treeWidget_itemDoubleClicked(QTreeWidgetItem *item, int column);
    void on_check_show_crosshair_clicked(bool checked);
    void on_radio_no_rot_clicked(bool checked);
    void on_radio_rot_clicked(bool checked);
    void on_radio_two_rot_clicked(bool checked);
//    void on_check_paint_clicked(bool checked);
    void on_button_reset_camera_clicked();
//    void on_check_paint_toggled(bool checked);

    void on_button_expand_clicked();

    void on_groupBox_3_toggled(bool checked);

    void on_spin_V_valueChanged(int arg1);

    void on_spin_F_valueChanged(int arg1);

    void on_spin_H_valueChanged(int arg1);

    void on_pushButton_clicked();

    void on_radio_brush_toggled(bool checked);

    void on_brush_size_valueChanged(int val);

    void on_Open_clicked();

    void on_MClose_button_clicked();

    void on_MDilate_button_clicked();

    void on_MErode_button_clicked();

signals:
    void paint(bool paint);
    void lang(QString lang);
    void compute(int size, bool dual);
    void changePaint(int mode);
    void brushSize(int size);
private:
    void loadLanguage(const QString& rLanguage);
private:
    mitk::Image::Pointer                                image2;
    char                                                originGeometry[sizeof(mitk::BaseGeometry)];
    Ui::MainWindow                                      *ui;
    QTranslator                                         m_translator; // contains the translations for this application
    QTranslator                                         m_translatorQt; // contains the translations for qt
    QString                                             m_currLang; // contains the currently loaded language
    QString                                             m_filetype;
};

#endif // MAINWINDOW_H
