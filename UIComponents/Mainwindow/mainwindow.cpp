#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QCloseEvent>
#include <qmessagebox.h>
#include <qdatetime.h>
#include <QFileDialog>
#include <qdebug.h>

#include "QmitkRenderWindow.h"
#include "QmitkSliceWidget.h"
#include "mitkProperties.h"
#include "mitkRenderingManager.h"
#include "mitkPointSet.h"
#include "mitkImageAccessByItk.h"
#include <mitkIOUtil.h>
#include <mitkInteractionConst.h>
#include <mitkProperties.h>
#include <mitkRenderingManager.h>
#include <mitkSurface.h>
#include "mitkDicomSeriesReader.h"
#include "mitkRotationOperation.h"
#include "mitkImagePixelWriteAccessor.h"
#include "mitkNumericTypes.h"

#include "vtkRenderer.h"
#include <vtkImageData.h>
#include <vtkMarchingCubes.h>
#include <vtkSTLWriter.h>

//forward declared
#include "customisedwidget.h"
#include "Algorythms/Painter/painting.h"
#include "IO/Handler/iohandler.h"
#include "Algorythms/vectors/vector.h"

MainWindow::MainWindow(QWidget *parent) :
    interactor(mitk::PointSetDataInteractor::New())
  ,  QMainWindow(parent)
//  ,  m_painter (nullptr)
  ,  ui(new Ui::MainWindow)
  , isabsrot(false)
  , IMGRVectors(new QList<mitk::Vector3D>())
  , RotAngles(new QList<double>())
  , RotVectors(new QList<mitk::Vector3D>())
  , n_slides(0)
  , binary_steps(0)
  , s_brush(0)
{
    ui->setupUi(this);
    connect(ui->actionopen_file, &QAction::triggered, this, &MainWindow::Open);
    CreateLunguages();

    connect(ui->actionopen_DICOM, &QAction::triggered, this, &MainWindow::ConvertToNRRD);
    ui->verticalLayout_12->setEnabled(false);
    ui->treeWidget->setColumnCount(4);
    ui->treeWidget->setColumnHidden(3,true);
    ui->treeWidget->setColumnWidth(0,120);
    ui->treeWidget->setColumnWidth(2,20);
//    connect(ui->view2, SIGNAL(wWasClicked(mitk::Point3D)), this, SLOT(windowClicked(mitk::Point3D)));
//    connect(ui->view2, SIGNAL(wWasDraged(mitk::Point3D)), this, SLOT(windowDraged(mitk::Point3D)));
//    connect(ui->view2, SIGNAL(wWasReleased()), this, SLOT(windowReleased()));
    connect(this, SIGNAL(paint(bool)),ui->view2, SIGNAL(paint(bool)));
    connect(this, SIGNAL(compute(int, bool)),ui->view2, SLOT(brset(int, bool)));
    connect(this, SIGNAL(lang(QString)),ui->view2, SLOT(lang(QString)));
    connect(this, SIGNAL(changePaint(int)),ui->view2, SIGNAL(changePaint(int)));
    connect(this, SIGNAL(brushSize(int)),ui->view2, SIGNAL(brushSize(int)));

}
MainWindow::~MainWindow()
{
    iohandler->clean();
    delete ui;
}
void MainWindow::Open(){

    //    QString folderNameDCM = QFileDialog::getExistingDirectory(this,tr("Open NRRD Folder"),QDir::currentPath(),QFileDialog::ShowDirsOnly);
    QString selectedFilter;
    QString fileNameDCM =  QFileDialog::getOpenFileName(this,tr("Open file"),QDir::currentPath(),
                           tr("NRRD Files (*.nrrd );;VTK Files (*.vtk );;Dicom Files(*);;STL Files (*.stl )"),&selectedFilter);
    if (fileNameDCM.isEmpty()){}
    else{
        if (selectedFilter == ""){
            selectedFilter = "dicom";
        }
        stdstrFolderNameDCM = fileNameDCM.toUtf8().constData();
        //    stdstrFolderNameDCM
        char *  path = &stdstrFolderNameDCM[0];
        qDebug()<<fileNameDCM;
        Load(path);
        Initialize();
        if (GetTreeDate(QDate::currentDate().toString("dd/MM/yyyy"),selectedFilter.split(" ")[0],fileNameDCM)){
            iohandler->sql->SQLInsertSession(selectedFilter.split(" ")[0],"1",fileNameDCM,iohandler->current_user);                 //chronione przed duplikatami
        }
        if (selectedFilter.split(" ")[0]=="STL"){
            ui->groupBox_6->setEnabled(false);
            ui->verticalLayout_12->setEnabled(false);
            m_filetype="STL";
            ui->tabWidget_2->setEnabled(false);
            ui->tabWidget->setTabEnabled(2,false);
        } else {
            ui->groupBox_6->setEnabled(true);
            ui->verticalLayout_12->setEnabled(true);
            m_filetype="standard";
            ui->tabWidget_2->setEnabled(true);
            ui->tabWidget->setTabEnabled(2,true);
        }
    }
}
void MainWindow::ConvertToNRRD(){

    QString folderNameDCM = QFileDialog::getExistingDirectory(this,tr("Open Folder"),QDir::currentPath(),QFileDialog::ShowDirsOnly);
    if (folderNameDCM.isEmpty()){}
    else{
        QString result = iohandler->openDicom(folderNameDCM);
        if(result.compare("error",Qt::CaseInsensitive)){
            qDebug()<<result.compare("error",Qt::CaseInsensitive);
        }
        else{
            qDebug()<<result;
            stdstrFolderNameDCM = (QDir::currentPath()+result).toUtf8().constData();
            char *  path = &stdstrFolderNameDCM[0];
            Load(path);
            Initialize();
        }}
}

//inits
void MainWindow::Initialize()
{
    geo = iohandler->getTimeGeometry();
//    iohandler-
    mitk::RenderingManager::GetInstance()->InitializeViews(geo);
    mitk::RenderingManager::GetInstance()->RequestUpdateAll();

    ui->view2->EnableStandardLevelWindow();

    // Add the displayed views to the DataStorage to see their positions in 2D and 3D
//     ui->view2->changeLayoutToBig3D();
    ui->view2->AddDisplayPlaneSubTree();
    ui->view2->AddPlanesToDataStorage();
    ui->view2->SetWidgetPlanesVisibility(true);
    if (m_FirstImage.IsNull())
        ui->verticalLayout_12->setEnabled(false);
    else{
        ui->verticalLayout_12->setEnabled(true);
    }

    // as in Step5, create PointSet (now as a member m_Seeds) and
    // associate a interactor to it

    m_Seeds = mitk::PointSet::New();
    mitk::DataNode::Pointer pointSetNode = mitk::DataNode::New();
    pointSetNode->SetData(m_Seeds);
    pointSetNode->SetProperty("layer", mitk::IntProperty::New(2));
    iohandler->getDataStorage()->Add(pointSetNode);

    // Create PointSetDataInteractor

    interactor->LoadStateMachine("PointSet.xml");
    interactor->SetEventConfig("PointSetConfig.xml");
    interactor->SetDataNode(pointSetNode);


    if (m_filetype=="STL"){
        ui->groupBox_6->setEnabled(false);
        ui->verticalLayout_12->setEnabled(false);
        ui->tabWidget_2->setEnabled(false);
        ui->tabWidget->setTabEnabled(2,false);
    } else {
        ui->groupBox_6->setEnabled(true);
        ui->verticalLayout_12->setEnabled(true);
        ui->tabWidget_2->setEnabled(true);
        ui->tabWidget->setTabEnabled(2,true);
    }


}
void MainWindow::Load(char *  path)
{
    //*************************************************************************
    // Part I: Basic initialization
    //*************************************************************************
    m_ResultNode = NULL;
    m_ResultImage = NULL;
    iohandler->resetDataStorage();
    ui->verticalLayout_12->setEnabled(true);
    ui->button_export_surface->setEnabled(false);
    if (RotVectors->isEmpty()){

    }else {
        RotVectors->clear();
        RotAngles->clear();
    }

    //*************************************************************************
    // Part II: Create some data by reading files
    //*************************************************************************

    // Load datanode (eg. many image formats, surface formats, etc.)
    mitk::StandaloneDataStorage::SetOfObjects::Pointer dataNodes = iohandler->LoadDataNodes(path);

    if (dataNodes->empty())
    {
        fprintf(stderr, "Could not open file %s \n\n", path);
        //      exit(2);
    }
    m_fdataNode = dataNodes->at(0);

    mitk::Image::Pointer image = dynamic_cast<mitk::Image *>(m_fdataNode->GetData());


//    AccessByItk_1(image, Smoothing, this);



    m_FirstImage = image;
    ui->view2->SetDataStorage(iohandler->getDataStorage());
     if((m_FirstImage.IsNotNull())
//             &(m_filetype!="STL")
             ){
     ui->view2->initImage(m_FirstImage);
     Initvect[0] = m_FirstImage->GetGeometry()->GetAxisVector(0);
     Initvect[1] = m_FirstImage->GetGeometry()->GetAxisVector(1);
     Initvect[2] = m_FirstImage->GetGeometry()->GetAxisVector(2);
     }
    SetupNormal();
}
void MainWindow::SetupNormal()
{
    ui->view2->setEnabled(true);
    ui->groupBox->setEnabled(true);
    ui->groupBox_3->setEnabled(true);
    ui->check_show_crosshair->setEnabled(true);
    InitGUI();
//    ui->groupBox->setEnabled(true);
//    ui->verticalLayout_12->setEnabled(true);

    LastPosition[0] = 0;
    LastPosition[1] = 0;
    LastPosition[2] = 0;
    SetAxisLabelH(0);
    SetAxisLabelV(0);
    SetAxisLabelF(0);
    ui->radioButtonAbs->setChecked(true);
    ui->radioButtonRel->setChecked(false);
    ui->radio_no_rot->setChecked(true);
    ui->axis_slider_f->setValue(0);
    ui->axis_slider_v->setValue(0);
    ui->axis_slider_h->setValue(0);
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j) {
            RotationVectors[i][j]= 0;
        }
        RotationVectors[i][i]=1;
    }

    changed[0] = 0;
    changed[1] = 0;
    changed[2] = 0;
    RotAxis = 4;
//    if (m_painter){
//        delete m_painter;
//    }
//    if(m_FirstImage.IsNotNull())
//    m_painter = new Painting(m_FirstImage);
    isabsrot = false;
    RotVectors->clear();
    RotAngles->clear();
    IMGRVectors->clear();

    ui->button_expand->setEnabled(false);
    ui->dual_box->setEnabled(false);
    ui->radio_brush->setChecked(true);
    ui->slice_or_brush->setText(tr("Brush size"));
    emit changePaint(1);
}

//ṣetters
void MainWindow::SetAxisLabelH(int position) {
    ui->spin_H->setValue(position);
}
void MainWindow::SetAxisLabelF(int position) {
    ui->spin_F->setValue(position);
}
void MainWindow::SetAxisLabelV(int position) {
    ui->spin_V->setValue(position);
}
void MainWindow::setIOHandler(IOHandler *handler){

    iohandler = handler;
    QList<QStringList> list = iohandler->sql->SQLSelectSession(iohandler->current_user);
    for (int var = 0; var < list.length(); ++var) {
        GetTreeDate(list[var][0], list[var][1], list[var][2]);
    }
    readSettings();
}

//session recorder
bool MainWindow::GetTreeDate(QString date, QString type, QString Path){
    QList<QTreeWidgetItem*> list = ui->treeWidget->findItems(date,Qt::MatchExactly);
    if (list.isEmpty()){
            QTreeWidgetItem *item = new QTreeWidgetItem();
            item->setText(0,date);
            ui->treeWidget->addTopLevelItem(item);

            return GetTreeType(item, type, Path);

    } else{
            return GetTreeType(list[0], type, Path);
    }

}
bool MainWindow::GetTreeType(QTreeWidgetItem *parent, QString type, QString Path){

bool ifer = true;
    for (int var = 0; var < parent->childCount(); ++var) {
        if(parent->child(var)->text(1)==type){
            return GetTreeChild(parent->child(var), Path);
            ifer = false;
            break;
        }
    }
    if (ifer){
        QTreeWidgetItem *item = new QTreeWidgetItem();
        item->setText(1,type);
        parent->addChild(item);
        return GetTreeChild(item, Path);

    }
}
bool MainWindow::GetTreeChild(QTreeWidgetItem *parent, QString Path){


    bool ifer = true;
        for (int var = 0; var < parent->childCount(); ++var) {
            if(parent->child(var)->text(3)==Path){
                callInfo(tr("You have already looked this file up today as: %1").arg(var+1));
                ifer = false;
                break;
            }
        }
        if (ifer){
            QTreeWidgetItem *item = new QTreeWidgetItem();
            item->setText(2,QString::number(1+parent->childCount()));
            item->setText(3,Path);
            parent->addChild(item);
        }
        return ifer;
}
void MainWindow::on_treeWidget_itemDoubleClicked(QTreeWidgetItem *item, int column){
    if (item->childCount()==0){
            stdstrFolderNameDCM = item->text(3).toUtf8().constData();
        char *  path = &stdstrFolderNameDCM[0];
        Load(path);
        Initialize();
        if (item->parent()->text(1)=="STL"){
            m_filetype="STL";
            ui->groupBox_6->setEnabled(false);
            ui->verticalLayout_12->setEnabled(false);
            ui->tabWidget_2->setEnabled(false);
            ui->tabWidget->setTabEnabled(1,false);
        } else {
            m_filetype="standard";
            ui->groupBox_6->setEnabled(true);
            ui->verticalLayout_12->setEnabled(true);
            ui->tabWidget_2->setEnabled(true);
            ui->tabWidget->setTabEnabled(1,true);
        }
    }
}

//opening and closing window
void MainWindow::closeEvent (QCloseEvent *event)
{
    if ((size()==iohandler->getSize())&&(pos()==iohandler->getPosition())&&(m_currLang==iohandler->getLunguage())){
        event->accept();
    }
    else {

        QMessageBox resBtn (QMessageBox::Question,
                            tr("Closing window"),
                            tr("Do you want to save settings?\n"),
                            QMessageBox::Cancel | QMessageBox::No | QMessageBox::Yes);
        resBtn.setButtonText(QMessageBox::Yes, tr("Yes"));
        resBtn.setButtonText(QMessageBox::No, tr("No"));
        resBtn.setButtonText(QMessageBox::Cancel, tr("Cancel"));
        int clicked = resBtn.exec();
        if (clicked == QMessageBox::Yes) {
            writeSettings();
            event->accept();
        } else if (clicked == QMessageBox::No){
            event->accept();
        } else if (clicked == QMessageBox::Cancel){
        event->ignore();
        }
    }
}
void MainWindow::writeSettings()
{
    iohandler->setSize(size());
    iohandler->setPosition(pos());
    iohandler->setLunguage(m_currLang);
    iohandler->writeSettings();
}
void MainWindow::readSettings()
{
    iohandler->readSettings();
    resize(iohandler->getSize());
    move(iohandler->getPosition());
    loadLanguage(iohandler->getLunguage());
}

void MainWindow::InitGUI()
{
    n_slides = 0;
    s_brush = 0;
    ui->brush_size->setValue(0);
    ui->radio_compute->setChecked(true);
    ui->groupBox_3->setChecked(false);
    binary_steps=0;
}

//lunguages
void MainWindow::CreateLunguages(){

    QActionGroup* langGroup = new QActionGroup(ui->menuLanguage);
    langGroup->setExclusive(true);
    connect(langGroup, SIGNAL (triggered(QAction *)), this, SLOT (slotLanguageChanged(QAction *)));
    QString defaultLocale = QLocale::system().name(); // e.g. "de_DE"
    defaultLocale.truncate(defaultLocale.lastIndexOf('_')); // e.g. "de"
    QString path = QApplication::applicationDirPath();
    QDir dir(path);
    QStringList fileNames = dir.entryList(QStringList("trans_*.qm"));

    for (int i = 0; i < fileNames.size(); ++i) {

        // get locale extracted by filename
        QString locale;
        locale = fileNames[i]; // "TranslationExample_de.qm"
        locale.truncate(locale.lastIndexOf('.')); // "TranslationExample_de"
        locale.remove(0, locale.indexOf('_') + 1); // "de"
        qDebug()<<locale;

        QString lang = QLocale::languageToString(QLocale(locale).language());
        QIcon ico(QString("%1/%2.png").arg(path+QString("/Resources")).arg(locale));

        QAction *action = new QAction(ico, lang, this);
        action->setCheckable(true);
        action->setData(locale);

        ui->menuLanguage->addAction(action);
        langGroup->addAction(action);

        // set default translators and language checked
        if (defaultLocale == locale)
        {
        action->setChecked(true);
        }
    }

}
void MainWindow::slotLanguageChanged(QAction* action)
{
 if(0 != action) {
  // load the language dependant on the action content
  loadLanguage(action->data().toString());
  setWindowIcon(action->icon());
 }
}
void MainWindow::switchTranslator(QTranslator& translator, const QString& filename)
{
 // remove the old translator
 qApp->removeTranslator(&translator);

 // load the new translator
 if(translator.load(filename))
  qApp->installTranslator(&translator);
}
void MainWindow::loadLanguage(const QString& rLanguage)
{
 if(m_currLang != rLanguage) {
  m_currLang = rLanguage;
  QLocale locale = QLocale(m_currLang);
  QLocale::setDefault(locale);
  QString languageName = QLocale::languageToString(locale.language());
  switchTranslator(m_translator, QString("trans_%1.qm").arg(rLanguage));
  switchTranslator(m_translatorQt, QString("qt_%1.qm").arg(rLanguage));
  ui->statusbar->showMessage(tr("Current Language changed to %1").arg(languageName));
 }
}
void MainWindow::changeEvent(QEvent* event)
{
 if(0 != event) {
  switch(event->type()) {
   // this event is send if a translator is loaded
   case QEvent::LanguageChange:
    ui->retranslateUi(this);
    emit lang(m_currLang);
    break;

   // this event is send, if the system, language changes
   case QEvent::LocaleChange:
   {
    QString locale = QLocale::system().name();
    locale.truncate(locale.lastIndexOf('_'));
    loadLanguage(locale);
    emit lang(m_currLang);
   }
   break;
  }
 }
 QMainWindow::changeEvent(event);
}

//rotate data
void MainWindow::DataRotator(int number, double position){
    mitk::BaseGeometry *geometry = m_FirstImage->GetGeometry();
    if (RotAxis==4){
        RotAxis=number;
    } else if ((RotAxis!=number)||(isabsrot != ui->radioButtonAbs->isChecked())){
        if (isabsrot){
            RotVectors->append(RotationVectors[RotAxis]);

            if (m_ResultImage.IsNotNull()){
            IMGRVectors->append(RotationVectors[RotAxis]);
            }
        }
        else {
            RotVectors->append(geometry->GetAxisVector(RotAxis));
            if (m_ResultImage.IsNotNull()){
            IMGRVectors->append(m_ResultImage->GetGeometry()->GetAxisVector(RotAxis));
            }
        }
        RotAngles->append(changed[RotAxis]-LastPosition[RotAxis]);
        changed[RotAxis] = LastPosition[RotAxis];
        RotAxis = number;
        isabsrot = ui->radioButtonAbs->isChecked();
    }

            mitk::RotationOperation *op;
            if (ui->radioButtonAbs->isChecked()){
                op = new mitk::RotationOperation(mitk::OpROTATE, geometry->GetCenter(), RotationVectors[number], position-LastPosition[number]);
                geometry->ExecuteOperation(op);
                op->~Operation();
            }else if(ui->radioButtonRel->isChecked()){
                op = new mitk::RotationOperation(mitk::OpROTATE, geometry->GetCenter(), geometry->GetAxisVector(number), position-LastPosition[number]);
                geometry->ExecuteOperation(op);
                op->~Operation();
            }


    if (m_ResultImage.IsNotNull()){
        mitk::BaseGeometry *igeometr = m_ResultImage->GetGeometry();
        if (ui->radioButtonAbs->isChecked()){
            op = new mitk::RotationOperation(mitk::OpROTATE, igeometr->GetCenter(), RotationVectors[number], position-LastPosition[number]);
        igeometr->ExecuteOperation(op);
        op->~Operation();
        }else if(ui->radioButtonRel){
            op = new mitk::RotationOperation(mitk::OpROTATE, igeometr->GetCenter(), igeometr->GetAxisVector(number), position-LastPosition[number]);
            igeometr->ExecuteOperation(op);
            op->~Operation();
        }
    }

    mitk::RenderingManager::GetInstance()->ForceImmediateUpdateAll();
    LastPosition[number] = position;
}
void MainWindow::on_axis_slider_v_sliderMoved(int position)
{
    DataRotator(2,(double) position);
    SetAxisLabelV(position);
}
void MainWindow::on_axis_slider_f_sliderMoved(int position)
{
    DataRotator(1,(double) position);
    SetAxisLabelF(position);
}
void MainWindow::on_axis_slider_h_sliderMoved(int position)
{
    DataRotator(0,(double) position);
    SetAxisLabelH(position);
}

//slots
void MainWindow::on_button_reload_clicked()
{
    Load(&stdstrFolderNameDCM[0]);
    //    this->SetupWidgets();
    Initialize();
}
void MainWindow::on_button_grow_clicked()
{

    AccessByItk_1(m_FirstImage, RegionGrowing, this);
//    m_FirstImage->get

    mitk::RenderingManager::GetInstance()->RequestUpdateAll();

    ui->button_export_surface->setEnabled(true);
}
void MainWindow::on_get_slicer_values_clicked()
{
    float wind = (ui->view2->levelWindowWidget->LineEditLevelWindowWidget->m_WindowInput->text().toFloat()/2);
    float level =  (ui->view2->levelWindowWidget->LineEditLevelWindowWidget->m_LevelInput->text()).toFloat();
    ui->m_LineEditThresholdMax->setText(QString::number((int)(wind+level)));
    ui->m_LineEditThresholdMin->setText(QString::number((int)(level-wind)));
}
void MainWindow::on_button_export_surface_clicked()
{
    if (m_ResultImage.IsNotNull())
    {
        QString fileNameDCM = QFileDialog::getSaveFileName(this,tr("Export surface"),QDir::currentPath(),tr("STL Files (*.stl )"));
        if (fileNameDCM.split(".").length()==1){
            fileNameDCM +=".stl";
        }
        vtkMarchingCubes* surfaceCreator = vtkMarchingCubes::New();
        surfaceCreator->SetInputData(m_ResultImage->GetVtkImageData());
        surfaceCreator->SetValue(0, ui->export_SpinBox->value());
        surfaceCreator->Update();
        mitk::Surface::Pointer surface = mitk::Surface::New();
        surface->SetVtkPolyData(surfaceCreator->GetOutput()); //VTK6_TODO
        iohandler->exportSurface(surface,fileNameDCM);
    }
    else {
        callError(tr("no surface to export"));
    }
}
void MainWindow::on_check_show_crosshair_clicked(bool checked)
{
      if (mitk::RenderingManager::GetInstance()->IsInstantiated())
      {
        mitk::DataNode *n;
        if (this->ui->view2)
        {
          n = this->ui->view2->GetWidgetPlane1();
          if (n)
            n->SetVisibility(checked);
          n = this->ui->view2->GetWidgetPlane2();
          if (n)
            n->SetVisibility(checked);
          n = this->ui->view2->GetWidgetPlane3();
          if (n)
            n->SetVisibility(checked);
          mitk::RenderingManager::GetInstance()->RequestUpdateAll();
        }
      }

}
void MainWindow::on_radio_no_rot_clicked(bool checked)
{
    ui->view2->SetWidgetPlaneMode(ui->view2->MOUSE_MODE_SLICING);
}
void MainWindow::on_radio_rot_clicked(bool checked)
{
    ui->view2->SetWidgetPlaneMode(ui->view2->MOUSE_MODE_ROTATION);
}
void MainWindow::on_radio_two_rot_clicked(bool checked)
{
    ui->view2->SetWidgetPlaneMode(ui->view2->MOUSE_MODE_ROTATION_LINKED);
}
void MainWindow::on_button_reset_camera_clicked()
{
    if (m_filetype!="STL"){
        mitk::BaseGeometry *geometry = m_FirstImage->GetGeometry();
        Vector v = Vector();
            if (fabs(geometry->GetAxisVector(0)[0] - Initvect[0][0])>acceptable_angle_error){
                qDebug()<< geometry->GetAxisVector(0)[0]<<" 0 "<< Initvect[0][0];
                v.rotate(*geometry, Initvect[0], 0);
                if (m_ResultImage.IsNotNull()){
                    mitk::BaseGeometry *igeo = m_ResultImage->GetGeometry();
                        v.rotate(*igeo, Initvect[0], 0);
                }
            }
            if (fabs(geometry->GetAxisVector(1)[0] - Initvect[1][0])>acceptable_angle_error){
                qDebug()<< geometry->GetAxisVector(1)[0]<<" 1 "<< Initvect[1][0];
                v.rotate(*geometry, Initvect[1], 1);
                if (m_ResultImage.IsNotNull()){
                    mitk::BaseGeometry *igeo = m_ResultImage->GetGeometry();
                        v.rotate(*igeo, Initvect[1], 1);
                }
            }
            if (fabs(geometry->GetAxisVector(2)[0] - Initvect[2][0])>acceptable_angle_error){
                qDebug()<< geometry->GetAxisVector(2)[0]<<" 2 "<< Initvect[2][0];
                v.rotate(*geometry, Initvect[2], 2);
                if (m_ResultImage.IsNotNull()){
                    mitk::BaseGeometry *igeo = m_ResultImage->GetGeometry();
                        v.rotate(*igeo, Initvect[2], 2);
                }
            }

    }

    SetupNormal();
    ui->view2->ResetCrosshair();
}
void MainWindow::on_button_expand_clicked()
{
    emit compute(ui->brush_size->value(),ui->dual_box->isChecked());
}

void MainWindow::on_groupBox_3_toggled(bool checked)
{
    ui->radio_brush->setEnabled(checked);
    ui->radio_compute->setEnabled(checked);
    ui->verticalLayout_10->setEnabled(checked);

    emit paint(checked);
}

//getting threasholds
int MainWindow::GetThresholdMin()
{
    return ui->m_LineEditThresholdMin->text().toInt();
}
int MainWindow::GetThresholdMax()
{
    return ui->m_LineEditThresholdMax->text().toInt();
}

//messages
void MainWindow::callError(QString error) {

    QMessageBox messageBox;
    messageBox.critical(0,tr("Error"),error);
    messageBox.setFixedSize(500,200);
}
void MainWindow::callWarning(QString warn) {
    QMessageBox messageBox;
    messageBox.warning(0,tr("Warning"),warn);
    messageBox.setFixedSize(500,200);
}
void MainWindow::callInfo(QString info) {
    QMessageBox messageBox;
    messageBox.information(0,tr("Notification"),info);
    messageBox.setFixedSize(500,200);
}

void MainWindow::on_spin_V_valueChanged(int arg1)
{
    DataRotator(2,(double) arg1);
    ui->axis_slider_v->setValue(arg1);
}

void MainWindow::on_spin_F_valueChanged(int arg1)
{
    DataRotator(1,(double) arg1);
    ui->axis_slider_f->setValue(arg1);
}

void MainWindow::on_spin_H_valueChanged(int arg1)
{
    DataRotator(0,(double) arg1);
    ui->axis_slider_h->setValue(arg1);
}

void MainWindow::on_pushButton_clicked()
{
     if (ui->radioButton_origin->isChecked()){
         AccessByItk_1(m_FirstImage, Smoothing, this);
         ui->view2->SetDataStorage(iohandler->getDataStorage());
          if(m_FirstImage.IsNotNull())
          ui->view2->initImage(m_FirstImage);
          mitk::RenderingManager::GetInstance()->RequestUpdateAll();
     }
     else if (ui->radioButton_Reconstruct->isChecked()){
         AccessByItk_1(m_ResultImage, SmoothingR, this);
         ui->view2->SetDataStorage(iohandler->getDataStorage());
          if(m_ResultImage.IsNotNull())
          ui->view2->initImage(m_ResultImage);
          mitk::RenderingManager::GetInstance()->RequestUpdateAll();
     }



}

void MainWindow::on_Open_clicked()
{
    binary_steps = ui->binary_steps_count->value();
    if (ui->radioButton_origin->isChecked()){
        AccessByItk_1(m_FirstImage, Opening, this);
//        ui->view2->SetDataStorage(iohandler->getDataStorage());
//         if(m_FirstImage.IsNotNull())
//         ui->view2->initImage(m_FirstImage);
         mitk::RenderingManager::GetInstance()->RequestUpdateAll();
    }
    else if (ui->radioButton_Reconstruct->isChecked()){
        if (m_ResultImage.IsNotNull())
        {
            AccessByItk_1(m_ResultImage, OpeningR, this);
//            ui->view2->SetDataStorage(iohandler->getDataStorage());
//             if(m_ResultImage.IsNotNull())
//             ui->view2->initImage(m_ResultImage);
             mitk::RenderingManager::GetInstance()->RequestUpdateAll();
        }
        else {
            callError(tr("no surface to export"));
        }
    }
}



void MainWindow::on_radio_brush_toggled(bool checked)
{
    if (checked)
    {
        ui->button_expand->setEnabled(false);
        ui->dual_box->setEnabled(false);
        ui->slice_or_brush->setText(tr("Brush size"));
        emit changePaint(1);
        n_slides = ui->brush_size->value();
        ui->brush_size->setValue(s_brush);
    }
    else
    {
        ui->button_expand->setEnabled(true);
        ui->dual_box->setEnabled(true);
        ui->slice_or_brush->setText(tr("Slices count"));
        emit changePaint(0);
        s_brush = ui->brush_size->value();
        ui->brush_size->setValue(n_slides);
    }
}

void MainWindow::on_brush_size_valueChanged(int val)
{
    emit brushSize(val);
}


void MainWindow::on_MClose_button_clicked()
{

    binary_steps = ui->binary_steps_count->value();
    if (ui->radioButton_origin->isChecked()){
        AccessByItk_1(m_FirstImage, Closing, this);
//        ui->view2->SetDataStorage(iohandler->getDataStorage());
//         if(m_FirstImage.IsNotNull())
//         ui->view2->initImage(m_FirstImage);
         mitk::RenderingManager::GetInstance()->RequestUpdateAll();
    }
    else if (ui->radioButton_Reconstruct->isChecked()){
        if (m_ResultImage.IsNotNull())
        {
            AccessByItk_1(m_ResultImage, ClosingR, this);
//            ui->view2->SetDataStorage(iohandler->getDataStorage());
//             if(m_ResultImage.IsNotNull())
//             ui->view2->initImage(m_ResultImage);
             mitk::RenderingManager::GetInstance()->RequestUpdateAll();
        }
        else {
            callError(tr("no surface to export"));
        }
    }
}
void MainWindow::on_MDilate_button_clicked()
{

    binary_steps = ui->binary_steps_count->value();
    if (ui->radioButton_origin->isChecked()){
        AccessByItk_1(m_FirstImage, Dilate, this);
//        ui->view2->SetDataStorage(iohandler->getDataStorage());
//         if(m_FirstImage.IsNotNull())
//         ui->view2->initImage(m_FirstImage);
         mitk::RenderingManager::GetInstance()->RequestUpdateAll();
    }
    else if (ui->radioButton_Reconstruct->isChecked()){
        if (m_ResultImage.IsNotNull())
        {
            AccessByItk_1(m_ResultImage, DilateR, this);
//            ui->view2->SetDataStorage(iohandler->getDataStorage());
//             if(m_ResultImage.IsNotNull())
//             ui->view2->initImage(m_ResultImage);
             mitk::RenderingManager::GetInstance()->RequestUpdateAll();
        }
        else {
            callError(tr("no surface to export"));
        }
    }
}
void MainWindow::on_MErode_button_clicked()
{

    binary_steps = ui->binary_steps_count->value();
    if (ui->radioButton_origin->isChecked()){
        AccessByItk_1(m_FirstImage, Erode, this);
//        ui->view2->SetDataStorage(iohandler->getDataStorage());
//         if(m_FirstImage.IsNotNull())
//         ui->view2->initImage(m_FirstImage);
         mitk::RenderingManager::GetInstance()->RequestUpdateAll();
    }
    else if (ui->radioButton_Reconstruct->isChecked()){

        if (m_ResultImage.IsNotNull())
        {
            AccessByItk_1(m_ResultImage, ErodeR, this);
//            ui->view2->SetDataStorage(iohandler->getDataStorage());
//             if(m_ResultImage.IsNotNull())
//             ui->view2->initImage(m_ResultImage);
             mitk::RenderingManager::GetInstance()->RequestUpdateAll();
        }
        else {
            callError(tr("no surface to export"));
        }
    }
}
