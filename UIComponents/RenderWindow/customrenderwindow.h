#ifndef CUSTOMRENDERWINDOW_H
#define CUSTOMRENDERWINDOW_H
#include <QmitkRenderWindow.h>
#include <mitkCameraController.h>
#include "mitkRenderWindowBase.h"

#include "QVTKWidget.h"
#include "QmitkRenderWindowMenu.h"
#include <MitkQtWidgetsExports.h>
#include <vtkGenericOpenGLRenderWindow.h>

#include "mitkBaseRenderer.h"
#include "mitkInteractionEventConst.h"
#include <QMouseEvent>
#include <QTranslator>
#include <mitkImage.h>

class Painting;

class CustomRenderWindow : public QmitkRenderWindow
{
    Q_OBJECT
public:
    CustomRenderWindow(
      QWidget *parent = 0,
      QString name = "unnamed renderwindow",
      mitk::VtkPropRenderer *renderer = NULL,
      mitk::RenderingManager *renderingManager = NULL,
      mitk::BaseRenderer::RenderingMode::Type renderingMode = mitk::BaseRenderer::RenderingMode::Standard);
    virtual ~CustomRenderWindow();
    mitk::Point3D ScreenToWorld(const mitk::Point2D &position);
    mitk::Point2D WorldToScreen(const mitk::Point3D &position);
    void InitPainter(mitk::Image::Pointer pointer);
    void destroyPainter();
    mitk::Image::Pointer getPainter();

protected:
    virtual void mousePressEvent(QMouseEvent *event) override;
    virtual void mouseMoveEvent(QMouseEvent *event) override;
    virtual void mouseReleaseEvent(QMouseEvent *event) override;
    virtual void wheelEvent(QWheelEvent *we) override;
    void inline Prepaint(mitk::Point3D position);
    void RunPathing(mitk::Point2D index);
    void Deloop(QList<mitk::Point2D> *list);
    void Pathfixing(QList<mitk::Point2D> *path, int i);
    void fixpaint(mitk::Point3D position);
    void countmedium();
    void countNewPath(QList<mitk::Point2D> previousPath, int pos);
    void slicePusher(int i);
    void denoiser(QList<mitk::Point2D> *previousPath);
    double checkNeighbour(mitk::Point2D pointer);
protected:
    mitk::BaseGeometry                                  *geometry;
    QList<mitk::Point2D>                                path;
    QList<mitk::Point2D>                                TempPath;
    QList<mitk::Point2D>                                PathToClear;
    mitk::Image::Pointer                                image;
    Painting                                            *m_painter;
    bool                                                sendmode;
    bool                                                dragging;
    QTranslator                                         m_translator; // contains the translations for this application
    int                                                 posinit;
    int                                                 medx;
    int                                                 medy;
    int                                                 brush;
    int                                                 paintmode;

public:
    int getBrush() const;
    void setBrush(int value);
    void computing(int range, bool dual);

public slots:
    void painting(bool ifpaint);
    void changePaint(int mode);
    void brushSize(int brush);
signals:
    void wasClicked(mitk::Point3D point);
    void wasDragged(mitk::Point3D point);
    void wasReleased();
};

#endif // CUSTOMRENDERWINDOW_H
