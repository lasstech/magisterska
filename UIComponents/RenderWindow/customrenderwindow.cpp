#include "customrenderwindow.h"
#include <QDebug>
#include <QEvent>
#include "mitkMouseMoveEvent.h"
#include "mitkMousePressEvent.h"
#include "mitkMouseReleaseEvent.h"
#include "mitkMouseWheelEvent.h"
#include "Algorythms/Painter/painting.h"

#define PI 3.14159265

CustomRenderWindow::CustomRenderWindow(
        QWidget *parent,
        QString name,
        mitk::VtkPropRenderer *renderer,
        mitk::RenderingManager *renderingManager,
        mitk::BaseRenderer::RenderingMode::Type renderingMode):
    QmitkRenderWindow(parent, name, renderer, renderingManager, renderingMode),
    paintmode(0)
{
    path = QList<mitk::Point2D>();
    if (m_painter){
        delete m_painter;
    }
    dragging = false;
}

CustomRenderWindow::~CustomRenderWindow(){
    delete this;
}

mitk::Point3D CustomRenderWindow::ScreenToWorld(const mitk::Point2D &position)
{
    mitk::Point3D pos;
    pos[0]= 0;
    pos[1]= 0;
    pos[2]= 0;
    m_Renderer->DisplayToWorld(position, pos);
    return pos;
}

mitk::Point2D CustomRenderWindow::WorldToScreen(const mitk::Point3D &position)
{
    mitk::Point2D pos;
    pos[0]= 0;
    pos[1]= 0;
    m_Renderer->WorldToDisplay(position, pos);
    return pos;
}

void CustomRenderWindow::InitPainter(mitk::Image::Pointer pointer)
{
    image = pointer;
    geometry = image->GetGeometry();
    m_painter = new Painting(pointer);
}

void CustomRenderWindow::destroyPainter()
{
    if (m_painter){
        delete m_painter;
    }
}

mitk::Image::Pointer CustomRenderWindow::getPainter()
{
    return image;
}


void CustomRenderWindow::fixpaint(mitk::Point3D position){

    itk::Index<3> index;
    geometry->WorldToIndex<3>(position, index);
    if (m_painter){
        m_painter->Paint(index);
    }
}

void CustomRenderWindow::countmedium()
{   medx=0;
    medy=0;
    foreach (mitk::Point2D var, path) {
        medx+=var[0];
        medy+=var[1];
    }
    medx= (int)medx/path.length();
    medy= (int)medy/path.length();
}

void CustomRenderWindow::countNewPath(QList<mitk::Point2D> previousPath, int pos)
{
    int go = GetSliceNavigationController()->GetSlice()->GetPos();
    GetSliceNavigationController()->GetSlice()->SetPos(go+pos);
    m_Renderer->ForceImmediateUpdate();

    QList<mitk::Point2D>                                newPath;
    mitk::Point2D                                       newpoint;
    double vmin;
    double lmin;
    int spectre = 3;
    for (int var = 0; var < previousPath.size(); var++) {
        mitk::Point2D pointer = previousPath[var];
        vmin = m_painter->getPixel(ScreenToWorld(pointer));
        newpoint = pointer;
        for (int rows = -spectre; rows < spectre; ++rows) {
            for (int collumns = -spectre; collumns < spectre; ++collumns) {
                if ((rows!=0)&&(collumns!=0)){
                    pointer[0] = previousPath[var][0]+rows;
                    pointer[1] = previousPath[var][1]+collumns;
                    lmin = m_painter->getPixel(ScreenToWorld(pointer));

                    if (lmin<vmin){
                        vmin = lmin;
                        newpoint = pointer;
                    }
                }
            }
        }
        if (!(previousPath.contains(pointer))){
        newPath.append(newpoint);}
    }
    GetSliceNavigationController()->GetSlice()->SetPos(go-pos);
    m_Renderer->ForceImmediateUpdate();
    foreach (mitk::Point2D var, previousPath) {
        m_painter->justpaint(ScreenToWorld(var));
    }
    m_painter->refresh();


    TempPath = newPath;
    GetSliceNavigationController()->GetSlice()->SetPos(go+pos);
    m_Renderer->ForceImmediateUpdate();

}

void CustomRenderWindow::slicePusher(int i)
{

    int go = GetSliceNavigationController()->GetSlice()->GetPos();
    GetSliceNavigationController()->GetSlice()->SetPos(go+1);
    m_Renderer->ForceImmediateUpdate();
}

void CustomRenderWindow::denoiser(QList<mitk::Point2D> *previousPath)
{
    for(int i =1; i<previousPath->size()-1; i++){
        if (     (abs(previousPath->at(i)[0]-previousPath->at(i-1)[0])>=3)
                &(abs(previousPath->at(i)[0]-previousPath->at(i+1)[0])>=3)
                &(abs(previousPath->at(i)[1]-previousPath->at(i-1)[0])>=3)
                &(abs(previousPath->at(i)[1]-previousPath->at(i+1)[0])>=3)){

            previousPath->removeAt(i);
        }
    }
}

double CustomRenderWindow::checkNeighbour(mitk::Point2D pointer)
{
    double lmin = 0;
    for (int var = -1; var <= 1; ++var) {
        for (int var2 = -1; var2 <= 1; ++var2) {
            mitk::Point2D     newpoint;

            newpoint[0] = pointer[0]+var;
            newpoint[1] = pointer[1]+var2;
            lmin += m_painter->getPixel(ScreenToWorld(newpoint));
        }

    }
    return lmin;
}

void CustomRenderWindow::setBrush(int value)
{
    brush = value;
}

int CustomRenderWindow::getBrush() const
{
    return brush;
}

void CustomRenderWindow::Prepaint(mitk::Point3D position)
{
    if (paintmode == 0){
        m_painter->justpaint(position);
    }
    else if (paintmode == 1){
        m_painter->RunPainting(position,brush);
    }
//    paintIt(position);
}

////Click event = MousePress->MouseRelease
void CustomRenderWindow::mouseReleaseEvent ( QMouseEvent * event )
{
    mitk::Point2D displayPos = GetMousePosition(event);
    mitk::MouseReleaseEvent::Pointer mReleaseEvent =
      mitk::MouseReleaseEvent::New(m_Renderer, displayPos, GetButtonState(event), GetModifiers(event), GetEventButton(event));
    if (!sendmode){
    if (!this->HandleEvent(mReleaseEvent.GetPointer()))
        {
          QVTKWidget::mouseReleaseEvent(event);
        }
    }
    if((event->button() == Qt::RightButton) && (sendmode))
    {
        dragging = false;


    }
}

void CustomRenderWindow::wheelEvent(QWheelEvent * we)
{
    mitk::Point2D displayPos = GetMousePosition(we);
    mitk::MouseWheelEvent::Pointer mWheelEvent =
      mitk::MouseWheelEvent::New(m_Renderer, displayPos, GetButtonState(we), GetModifiers(we), GetDelta(we));
    if (!this->HandleEvent(mWheelEvent.GetPointer()))
    {
      QVTKWidget::wheelEvent(we);
    }
//    if (m_ResendQtEvents)
    //      we->ignore();
}

void CustomRenderWindow::painting(bool ifpaint)
{
    sendmode = ifpaint;
}

void CustomRenderWindow::changePaint(int brushSize)
{
    paintmode = brushSize;
}

void CustomRenderWindow::brushSize(int brush)
{
    setBrush(brush);
}

void CustomRenderWindow::computing(int range, bool dual)
{
    if(!path.empty()){
        posinit = GetSliceNavigationController()->GetSlice()->GetPos();
        countmedium();
        Pathfixing(&path, 0);
        countNewPath(path,1);
        int go = GetSliceNavigationController()->GetSlice()->GetPos();

        for (int var = 0; var < range; ++var) {
            denoiser(&TempPath);
            Pathfixing(&TempPath, 0);
            Deloop(&TempPath);
            go = GetSliceNavigationController()->GetSlice()->GetPos();
            if (go >GetSliceNavigationController()->GetSlice()->GetRangeMax()-1) break;
            countNewPath(TempPath,1);
        }
        if (dual){
            go = posinit;
            GetSliceNavigationController()->GetSlice()->SetPos(go);
            m_Renderer->ForceImmediateUpdate();
            Pathfixing(&path, 0);
            countNewPath(path,-1);

            for (int var = 0; var < range; ++var) {
                denoiser(&TempPath);
                Pathfixing(&TempPath, 0);
                Deloop(&TempPath);
                go = GetSliceNavigationController()->GetSlice()->GetPos();
                if (go >GetSliceNavigationController()->GetSlice()->GetRangeMin()+1) break;
                countNewPath(TempPath,-1);
            }
        }
        TempPath.clear();
        path.clear();
        qDebug()<<path.length()<< "len";
    }
}

void CustomRenderWindow::mousePressEvent(QMouseEvent *event)
{
    path.clear();
    TempPath.clear();
    // Get mouse position in vtk display coordinate system. me contains qt display infos...
    mitk::Point2D displayPos = GetMousePosition(event);

    mitk::MousePressEvent::Pointer mPressEvent =
      mitk::MousePressEvent::New(m_Renderer, displayPos, GetButtonState(event), GetModifiers(event), GetEventButton(event));

    if (!sendmode){
        if (!this->HandleEvent(mPressEvent.GetPointer()))
        {
          QVTKWidget::mousePressEvent(event);
        }
    }
    if((event->button() == Qt::RightButton) && (sendmode))
    {
        dragging = true;
        Prepaint(mPressEvent->GetPositionInWorld());
    }
}

void CustomRenderWindow::mouseMoveEvent(QMouseEvent *event)
{
    mitk::Point2D displayPos = GetMousePosition(event);

    this->AdjustRenderWindowMenuVisibility(event->pos());

    mitk::MouseMoveEvent::Pointer mMoveEvent =
      mitk::MouseMoveEvent::New(m_Renderer, displayPos, GetButtonState(event), GetModifiers(event));

    if (!sendmode){
        if (!this->HandleEvent(mMoveEvent.GetPointer()))
        {
          QVTKWidget::mouseMoveEvent(event);
        }
    }
    if ((dragging) && (sendmode)){
        mitk::Point2D point  = mMoveEvent->GetPointerPositionOnScreen();
        RunPathing(point);
        Prepaint(ScreenToWorld(point));
        m_painter->refresh();
    }
    
}

void CustomRenderWindow::RunPathing(mitk::Point2D index)
{
    if (path.contains(index)){
        if (path.indexOf(index)<path.size()-path.indexOf(index)){
            int i=path.indexOf(index);
            while (i>0){
                path.removeAt(i);
                i--;
            }
        }
    }
    else {
        path.append(index);
    }
}

void CustomRenderWindow::Deloop(QList<mitk::Point2D> *list)
{
    for (int var = 0; var < list->length()-1; ++var) {
        for (int var2 = var+1; var2 < list->length(); ++var2) {

            if ((list->at(var)==list->at(var2))){
//                list->erase(var,var2);
//                var2=var;
//                qDebug()<<"iz";
                while (var!=var2){
                    list->removeAt(var2);
                    var2--;
                }
//                var2++;
            }
        }
    }
}

void CustomRenderWindow::Pathfixing(QList<mitk::Point2D> *pathToFix, int i)
{
    if (i < pathToFix->size()-1){
        qDebug() <<i;
        int a = pathToFix->at(i)[0]-pathToFix->at(i+1)[0];
        int b = pathToFix->at(i)[1]-pathToFix->at(i+1)[1];
        if (abs(a) >1||abs(b)>1){
            mitk::Point2D point;
            point[0]= (int)(pathToFix->at(i)[0] -(a/2));

            point[1]= (int)(pathToFix->at(i)[1] -(b/2));
            pathToFix->insert(i+1, point);
//            Prepaint(ScreenToWorld(point));

            Pathfixing(pathToFix, i);
        }else {
            qDebug() <<"i";
            Pathfixing(pathToFix, i+1);
        }
    } else{
    }
}
