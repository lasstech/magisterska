#include "glue.h"
#include "QmitkRegisterClasses.h"
#include "UIComponents/DialogLogin/dialoglogin.h"
#include "UIComponents/Mainwindow/mainwindow.h"

Glue::Glue(QObject *parent) : QObject(parent)
{

    handler = new IOHandler();
    login = new DialogLogin(handler);
    login->show();
    QObject::connect(login, &DialogLogin::veryfied, this, &Glue::on_login_success);


//    QmitkRegisterClasses();
//    handler->current_user = "loo";
//    mainwindow = new MainWindow();
//    mainwindow->setIOHandler(handler);
//    mainwindow->show();

}


void Glue::on_login_success(QString user){
    QmitkRegisterClasses();
    handler->current_user = user;
    mainwindow = new MainWindow();
    mainwindow->setIOHandler(handler);
    mainwindow->show();
    login->~DialogLogin();
}
