#ifndef GLUE_H
#define GLUE_H

#include <QObject>
#include "IO/Handler/iohandler.h"
//#include "UIComponents/DialogLogin/dialoglogin.h"
//#include "UIComponents/Mainwindow/mainwindow.h"
class DialogLogin;
class MainWindow;

class Glue : public QObject
{
    Q_OBJECT
public:
    explicit Glue(QObject *parent = 0);
private:
    IOHandler *handler;
    DialogLogin *login;
    MainWindow *mainwindow;

signals:
    void on_crash_success();

public slots:
    void on_login_success(QString user);

};

#endif // GLUE_H
