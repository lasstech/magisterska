
//#define SMW_INFO MITK_INFO("widget.stdmulti")

#include "customisedwidget.h"

#include <vtkTextProperty.h>

//#include <usModuleInitialization.h>
//US_INITIALIZE_MODULE




#include <QGridLayout>
#include <QHBoxLayout>
#include <QList>
#include <QMouseEvent>
#include <QTimer>
#include <QVBoxLayout>
#include <qsplitter.h>
#include <QDebug>

#include <vtkCornerAnnotation.h>
#include <vtkMitkRectangleProp.h>






CustomisedWidget::CustomisedWidget(QWidget *parent,
                                   Qt::WindowFlags f,
                                   mitk::RenderingManager *renderingManager,
                                   mitk::BaseRenderer::RenderingMode::Type renderingMode,
                                   const QString &name)
    :
    QWidget(parent, f),
        mitkWidget1(NULL),
        mitkWidget2(NULL),
        mitkWidget3(NULL),
        mitkWidget4(NULL),
        levelWindowWidget(NULL),
        QmitkStdMultiWidgetLayout(NULL),
        m_PlaneMode(PLANE_MODE_SLICING),
        m_RenderingManager(renderingManager),
        m_GradientBackgroundFlag(true),
        m_TimeNavigationController(NULL),
        m_MainSplit(NULL),
        m_LayoutSplit(NULL),
        m_SubSplit1(NULL),
        m_SubSplit2(NULL),
        mitkWidget1Container(NULL),
        mitkWidget2Container(NULL),
        mitkWidget3Container(NULL),
        mitkWidget4Container(NULL),
        m_PendingCrosshairPositionEvent(false),
        m_CrosshairNavigationEnabled(false)
{

    /******************************************************
     * Use the global RenderingManager if none was specified
     * ****************************************************/
    if (m_RenderingManager == NULL)
    {
      m_RenderingManager = mitk::RenderingManager::GetInstance();
    }
    m_TimeNavigationController = m_RenderingManager->GetTimeNavigationController();

    /*******************************/
    // Create Widget manually
    /*******************************/

    // create Layouts
    QmitkStdMultiWidgetLayout = new QHBoxLayout(this);
    QmitkStdMultiWidgetLayout->setContentsMargins(0, 0, 0, 0);

    // Set Layout to widget
    this->setLayout(QmitkStdMultiWidgetLayout);

    // create main splitter
    m_MainSplit = new QSplitter(this);
    QmitkStdMultiWidgetLayout->addWidget(m_MainSplit);

    // create m_LayoutSplit  and add to the mainSplit
    m_LayoutSplit = new QSplitter(Qt::Vertical, m_MainSplit);
    m_MainSplit->addWidget(m_LayoutSplit);

    // create m_SubSplit1 and m_SubSplit2
    m_SubSplit1 = new QSplitter(m_LayoutSplit);
    m_SubSplit2 = new QSplitter(m_LayoutSplit);

    // creae Widget Container
    mitkWidget1Container = new QWidget(m_SubSplit1);
    mitkWidget2Container = new QWidget(m_SubSplit1);
    mitkWidget3Container = new QWidget(m_SubSplit2);
    mitkWidget4Container = new QWidget(m_SubSplit2);

    mitkWidget1Container->setContentsMargins(0, 0, 0, 0);
    mitkWidget2Container->setContentsMargins(0, 0, 0, 0);
    mitkWidget3Container->setContentsMargins(0, 0, 0, 0);
    mitkWidget4Container->setContentsMargins(0, 0, 0, 0);

    // create Widget Layout
    QHBoxLayout *mitkWidgetLayout1 = new QHBoxLayout(mitkWidget1Container);
    QHBoxLayout *mitkWidgetLayout2 = new QHBoxLayout(mitkWidget2Container);
    QHBoxLayout *mitkWidgetLayout3 = new QHBoxLayout(mitkWidget3Container);
    QHBoxLayout *mitkWidgetLayout4 = new QHBoxLayout(mitkWidget4Container);

    m_CornerAnnotations[0] = vtkSmartPointer<vtkCornerAnnotation>::New();
    m_CornerAnnotations[1] = vtkSmartPointer<vtkCornerAnnotation>::New();
    m_CornerAnnotations[2] = vtkSmartPointer<vtkCornerAnnotation>::New();
    m_CornerAnnotations[3] = vtkSmartPointer<vtkCornerAnnotation>::New();

    m_RectangleProps[0] = vtkSmartPointer<vtkMitkRectangleProp>::New();
    m_RectangleProps[1] = vtkSmartPointer<vtkMitkRectangleProp>::New();
    m_RectangleProps[2] = vtkSmartPointer<vtkMitkRectangleProp>::New();
    m_RectangleProps[3] = vtkSmartPointer<vtkMitkRectangleProp>::New();

    mitkWidgetLayout1->setMargin(0);
    mitkWidgetLayout2->setMargin(0);
    mitkWidgetLayout3->setMargin(0);
    mitkWidgetLayout4->setMargin(0);

    // set Layout to Widget Container
    mitkWidget1Container->setLayout(mitkWidgetLayout1);
    mitkWidget2Container->setLayout(mitkWidgetLayout2);
    mitkWidget3Container->setLayout(mitkWidgetLayout3);
    mitkWidget4Container->setLayout(mitkWidgetLayout4);

    // set SizePolicy
    mitkWidget1Container->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    mitkWidget2Container->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    mitkWidget3Container->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    mitkWidget4Container->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    // insert Widget Container into the splitters
    m_SubSplit1->addWidget(mitkWidget1Container);
    m_SubSplit1->addWidget(mitkWidget2Container);

    m_SubSplit2->addWidget(mitkWidget3Container);
    m_SubSplit2->addWidget(mitkWidget4Container);

    // Create RenderWindows 1
    mitkWidget1 = new CustomRenderWindow(mitkWidget1Container, name + ".widget1", NULL, m_RenderingManager, renderingMode);
    mitkWidget1->SetLayoutIndex(AXIAL);
    mitkWidgetLayout1->addWidget(mitkWidget1);

    // Create RenderWindows 2
    mitkWidget2 = new CustomRenderWindow(mitkWidget2Container, name + ".widget2", NULL, m_RenderingManager, renderingMode);
    mitkWidget2->setEnabled(true);
    mitkWidget2->SetLayoutIndex(SAGITTAL);
    mitkWidgetLayout2->addWidget(mitkWidget2);

    // Create RenderWindows 3
    mitkWidget3 = new CustomRenderWindow(mitkWidget3Container, name + ".widget3", NULL, m_RenderingManager, renderingMode);
    mitkWidget3->SetLayoutIndex(CORONAL);
    mitkWidgetLayout3->addWidget(mitkWidget3);

    // Create RenderWindows 4
    mitkWidget4 = new CustomRenderWindow(mitkWidget4Container, name + ".widget4", NULL, m_RenderingManager, renderingMode);
    mitkWidget4->SetLayoutIndex(THREE_D);
    mitkWidgetLayout4->addWidget(mitkWidget4);

    connect(mitkWidget1, SIGNAL(wasClicked(mitk::Point3D)), this, SIGNAL(wWasClicked(mitk::Point3D)));
    connect(mitkWidget2, SIGNAL(wasClicked(mitk::Point3D)), this, SIGNAL(wWasClicked(mitk::Point3D)));
    connect(mitkWidget3, SIGNAL(wasClicked(mitk::Point3D)), this, SIGNAL(wWasClicked(mitk::Point3D)));

    connect(mitkWidget1, SIGNAL(wasDragged(mitk::Point3D)), this, SIGNAL(wWasDraged(mitk::Point3D)));
    connect(mitkWidget2, SIGNAL(wasDragged(mitk::Point3D)), this, SIGNAL(wWasDraged(mitk::Point3D)));
    connect(mitkWidget3, SIGNAL(wasDragged(mitk::Point3D)), this, SIGNAL(wWasDraged(mitk::Point3D)));

    connect(mitkWidget1, SIGNAL(wasReleased()), this, SIGNAL(wWasReleased()));
    connect(mitkWidget2, SIGNAL(wasReleased()), this, SIGNAL(wWasReleased()));
    connect(mitkWidget3, SIGNAL(wasReleased()), this, SIGNAL(wWasReleased()));

    connect(this, SIGNAL(paint(bool)), mitkWidget1, SLOT(painting(bool)));
    connect(this, SIGNAL(paint(bool)), mitkWidget2, SLOT(painting(bool)));
    connect(this, SIGNAL(paint(bool)), mitkWidget3, SLOT(painting(bool)));

    connect(this, SIGNAL(changePaint(int)), mitkWidget1, SLOT(changePaint(int)));
    connect(this, SIGNAL(changePaint(int)), mitkWidget2, SLOT(changePaint(int)));
    connect(this, SIGNAL(changePaint(int)), mitkWidget3, SLOT(changePaint(int)));

    connect(this, SIGNAL(brushSize(int)), mitkWidget1, SLOT(brushSize(int)));
    connect(this, SIGNAL(brushSize(int)), mitkWidget2, SLOT(brushSize(int)));
    connect(this, SIGNAL(brushSize(int)), mitkWidget3, SLOT(brushSize(int)));

    // Create Level Window Widget
    levelWindowWidget = new QmitkLevelWindowWidget(m_MainSplit); // this
    levelWindowWidget->setObjectName(QString::fromUtf8("levelWindowWidget"));
    QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    sizePolicy.setHorizontalStretch(0);
    sizePolicy.setVerticalStretch(0);
    sizePolicy.setHeightForWidth(levelWindowWidget->sizePolicy().hasHeightForWidth());
    levelWindowWidget->setSizePolicy(sizePolicy);
    levelWindowWidget->setMaximumWidth(50);
//    lev

    // add LevelWindow Widget to mainSplitter
    m_MainSplit->addWidget(levelWindowWidget);

    // show mainSplitt and add to Layout
    m_MainSplit->show();

    // resize Image.
    this->resize(QSize(364, 477).expandedTo(minimumSizeHint()));

    // Initialize the widgets.
    this->InitializeWidget();

    // Activate Widget Menu
//    this->ActivateMenuWidget(true);
}

 CustomisedWidget::~CustomisedWidget(){

 }





 void CustomisedWidget::InitializeWidget()
 {
   // Make all black and overwrite renderwindow 4
   this->FillGradientBackgroundWithBlack();
   // This is #191919 in hex
   float tmp1[3] = {0.098f, 0.098f, 0.098f};
   // This is #7F7F7F in hex
   float tmp2[3] = {0.498f, 0.498f, 0.498f};
   m_GradientBackgroundColors[3] = std::make_pair(mitk::Color(tmp1), mitk::Color(tmp2));

   // Yellow is default color for widget4
   m_DecorationColorWidget4[0] = 1.0f;
   m_DecorationColorWidget4[1] = 1.0f;
   m_DecorationColorWidget4[2] = 0.0f;

   // transfer colors in WorldGeometry-Nodes of the associated Renderer
   mitk::IntProperty::Pointer layer;
   // of widget 1

   m_PlaneNode1 = mitk::BaseRenderer::GetInstance(mitkWidget1->GetRenderWindow())->GetCurrentWorldPlaneGeometryNode();
   m_PlaneNode1->SetColor(GetDecorationColor(0));
   layer = mitk::IntProperty::New(1000);
   m_PlaneNode1->SetProperty("layer", layer);

   // ... of widget 2
   m_PlaneNode2 = mitk::BaseRenderer::GetInstance(mitkWidget2->GetRenderWindow())->GetCurrentWorldPlaneGeometryNode();
   m_PlaneNode2->SetColor(GetDecorationColor(1));
   layer = mitk::IntProperty::New(1000);
   m_PlaneNode2->SetProperty("layer", layer);

   // ... of widget 3
   m_PlaneNode3 = mitk::BaseRenderer::GetInstance(mitkWidget3->GetRenderWindow())->GetCurrentWorldPlaneGeometryNode();
   m_PlaneNode3->SetColor(GetDecorationColor(2));
   layer = mitk::IntProperty::New(1000);
   m_PlaneNode3->SetProperty("layer", layer);

   // The parent node
   m_ParentNodeForGeometryPlanes =
     mitk::BaseRenderer::GetInstance(mitkWidget4->GetRenderWindow())->GetCurrentWorldPlaneGeometryNode();
   layer = mitk::IntProperty::New(1000);
   m_ParentNodeForGeometryPlanes->SetProperty("layer", layer);



   InitialiseLabels();






   // connect to the "time navigation controller": send time via sliceNavigationControllers
   m_TimeNavigationController->ConnectGeometryTimeEvent(mitkWidget1->GetSliceNavigationController(), false);
   m_TimeNavigationController->ConnectGeometryTimeEvent(mitkWidget2->GetSliceNavigationController(), false);
   m_TimeNavigationController->ConnectGeometryTimeEvent(mitkWidget3->GetSliceNavigationController(), false);
   m_TimeNavigationController->ConnectGeometryTimeEvent(mitkWidget4->GetSliceNavigationController(), false);
   mitkWidget1->GetSliceNavigationController()->ConnectGeometrySendEvent(
     mitk::BaseRenderer::GetInstance(mitkWidget4->GetRenderWindow()));

   // reverse connection between sliceNavigationControllers and m_TimeNavigationController
   mitkWidget1->GetSliceNavigationController()->ConnectGeometryTimeEvent(m_TimeNavigationController, false);
   mitkWidget2->GetSliceNavigationController()->ConnectGeometryTimeEvent(m_TimeNavigationController, false);
   mitkWidget3->GetSliceNavigationController()->ConnectGeometryTimeEvent(m_TimeNavigationController, false);
   // mitkWidget4->GetSliceNavigationController()
   //  ->ConnectGeometryTimeEvent(m_TimeNavigationController, false);

   m_MouseModeSwitcher = mitk::MouseModeSwitcher::New();
 }

 void CustomisedWidget::InitialiseLabels()
 {
     mitk::BaseRenderer::GetInstance(mitkWidget4->GetRenderWindow())->SetMapperID(mitk::BaseRenderer::Standard3D);
     // Set plane mode (slicing/rotation behavior) to slicing (default)
     m_PlaneMode = PLANE_MODE_SLICING;
     // Set default view directions for SNCs
     mitkWidget1->GetSliceNavigationController()->SetDefaultViewDirection(mitk::SliceNavigationController::Axial);
     mitkWidget2->GetSliceNavigationController()->SetDefaultViewDirection(mitk::SliceNavigationController::Sagittal);
     mitkWidget3->GetSliceNavigationController()->SetDefaultViewDirection(mitk::SliceNavigationController::Frontal);
     mitkWidget4->GetSliceNavigationController()->SetDefaultViewDirection(mitk::SliceNavigationController::Original);


//     lang
//     SetDecorationProperties(tr("Axial").toStdString(), GetDecorationColor(0), 0);
//     SetDecorationProperties(tr("Sagittal").toStdString(), GetDecorationColor(1), 1);
//     SetDecorationProperties(tr("Coronal").toStdString(), GetDecorationColor(2), 2);
//     SetDecorationProperties("3D", GetDecorationColor(3), 3);
 }


// void CustomisedWidget::InicialiseColor(CustomRenderWindow *mitkWidget, mitk::DataNode::Pointer data, int number)
// {
//     mitk::IntProperty::Pointer layer;
//     data = mitk::BaseRenderer::GetInstance(mitkWidget->GetRenderWindow())->GetCurrentWorldPlaneGeometryNode();
//     data->SetColor(GetDecorationColor(number));
//     layer = mitk::IntProperty::New(1000);
//     data->SetProperty("layer", layer);

// }


 void CustomisedWidget::FillGradientBackgroundWithBlack()
 {
   // We have 4 widgets and ...
   for (unsigned int i = 0; i < 4; ++i)
   {
     float black[3] = {0.0f, 0.0f, 0.0f};
     m_GradientBackgroundColors[i] = std::make_pair(mitk::Color(black), mitk::Color(black));
   }
 }



 void CustomisedWidget::SetDecorationProperties(std::string text, mitk::Color color, int widgetNumber)
 {
   if (widgetNumber > 3)
   {
//     MITK_ERROR << "Unknown render window for annotation.";
     return;
   }
   vtkRenderer *renderer = this->GetRenderWindow(widgetNumber)->GetRenderer()->GetVtkRenderer();
   if (!renderer)
     return;
   vtkSmartPointer<vtkCornerAnnotation> annotation = m_CornerAnnotations[widgetNumber];
   annotation->SetText(0, text.c_str());
   annotation->SetMaximumFontSize(12);
   annotation->GetTextProperty()->SetColor(color[0], color[1], color[2]);
   if (!renderer->HasViewProp(annotation))
   {
     renderer->AddViewProp(annotation);
   }
   vtkSmartPointer<vtkMitkRectangleProp> frame = m_RectangleProps[widgetNumber];
   frame->SetColor(color[0], color[1], color[2]);
   if (!renderer->HasViewProp(frame))
   {
     renderer->AddViewProp(frame);
   }
 }



 mitk::Color CustomisedWidget::GetDecorationColor(unsigned int widgetNumber)
 {
   float tmp[3] = {0.0f, 0.0f, 0.0f};
   switch (widgetNumber)
   {
     case 0:
     {
       if (m_PlaneNode1.IsNotNull())
       {
         if (m_PlaneNode1->GetColor(tmp))
         {
           return dynamic_cast<mitk::ColorProperty *>(m_PlaneNode1->GetProperty("color"))->GetColor();
         }
       }
       float red[3] = {0.753f, 0.0f, 0.0f}; // This is #C00000 in hex
       return mitk::Color(red);
     }
     case 1:
     {
       if (m_PlaneNode2.IsNotNull())
       {
         if (m_PlaneNode2->GetColor(tmp))
         {
           return dynamic_cast<mitk::ColorProperty *>(m_PlaneNode2->GetProperty("color"))->GetColor();
         }
       }
       float green[3] = {0.0f, 0.69f, 0.0f}; // This is #00B000 in hex
       return mitk::Color(green);
     }
     case 2:
     {
       if (m_PlaneNode3.IsNotNull())
       {
         if (m_PlaneNode3->GetColor(tmp))
         {
           return dynamic_cast<mitk::ColorProperty *>(m_PlaneNode3->GetProperty("color"))->GetColor();
         }
       }
       float blue[3] = {0.0, 0.502f, 1.0f}; // This is #0080FF in hex
       return mitk::Color(blue);
     }
     case 3:
     {
       return m_DecorationColorWidget4;
     }
     default:
//       MITK_ERROR << "Decoration color for unknown widget!";
       float black[3] = {0.0f, 0.0f, 0.0f};
       return mitk::Color(black);
   }
 }



 QmitkRenderWindow *CustomisedWidget::GetRenderWindow(unsigned int number)
 {
   switch (number)
   {
     case 0:
       return this->GetRenderWindow1();
     case 1:
       return this->GetRenderWindow2();
     case 2:
       return this->GetRenderWindow3();
     case 3:
       return this->GetRenderWindow4();
     default:
//       MITK_ERROR << "Requested unknown render window";
       break;
   }
   return NULL;
 }

 void CustomisedWidget::initImage(mitk::Image::Pointer m_FirstImage)
 {
     widgetIterate(m_FirstImage, *mitkWidget1);
     widgetIterate(m_FirstImage, *mitkWidget2);
     widgetIterate(m_FirstImage, *mitkWidget3);
     widgetIterate(m_FirstImage, *mitkWidget4);
 }

 void CustomisedWidget::widgetIterate(mitk::Image::Pointer pointer, CustomRenderWindow &widget)
 {
     widget.InitPainter(pointer);
 }




 QmitkRenderWindow *CustomisedWidget::GetRenderWindow1() const
 {
   return mitkWidget1;
 }

 QmitkRenderWindow *CustomisedWidget::GetRenderWindow2() const
 {
   return mitkWidget2;
 }

 QmitkRenderWindow *CustomisedWidget::GetRenderWindow3() const
 {
   return mitkWidget3;
 }

 QmitkRenderWindow *CustomisedWidget::GetRenderWindow4() const
 {
   return mitkWidget4;
 }


 void CustomisedWidget::AddDisplayPlaneSubTree()
 {
   // add the displayed planes of the multiwidget to a node to which the subtree
   // @a planesSubTree points ...

////   mitk::PlaneGeometryDataMapper2D::Pointer mapper;

//   // ... of widget 1
//   mitk::BaseRenderer *renderer1 = mitk::BaseRenderer::GetInstance(mitkWidget1->GetRenderWindow());
//   m_PlaneNode1 = renderer1->GetCurrentWorldPlaneGeometryNode();
//   m_PlaneNode1->SetProperty("visible", mitk::BoolProperty::New(true));
//   m_PlaneNode1->SetProperty("name", mitk::StringProperty::New(std::string(renderer1->GetName()) + ".plane"));
//   m_PlaneNode1->SetProperty("includeInBoundingBox", mitk::BoolProperty::New(false));
//   m_PlaneNode1->SetProperty("helper object", mitk::BoolProperty::New(true));
////   mapper = mitk::PlaneGeometryDataMapper2D::New();
////   m_PlaneNode1->SetMapper(mitk::BaseRenderer::Standard2D, mapper);

//   // ... of widget 2
//   mitk::BaseRenderer *renderer2 = mitk::BaseRenderer::GetInstance(mitkWidget2->GetRenderWindow());
//   m_PlaneNode2 = renderer2->GetCurrentWorldPlaneGeometryNode();
//   m_PlaneNode2->SetProperty("visible", mitk::BoolProperty::New(true));
//   m_PlaneNode2->SetProperty("name", mitk::StringProperty::New(std::string(renderer2->GetName()) + ".plane"));
//   m_PlaneNode2->SetProperty("includeInBoundingBox", mitk::BoolProperty::New(false));
//   m_PlaneNode2->SetProperty("helper object", mitk::BoolProperty::New(true));
////   mapper = mitk::PlaneGeometryDataMapper2D::New();
////   m_PlaneNode2->SetMapper(mitk::BaseRenderer::Standard2D, mapper);

//   // ... of widget 3
//   mitk::BaseRenderer *renderer3 = mitk::BaseRenderer::GetInstance(mitkWidget3->GetRenderWindow());
//   m_PlaneNode3 = renderer3->GetCurrentWorldPlaneGeometryNode();
//   m_PlaneNode3->SetProperty("visible", mitk::BoolProperty::New(true));
//   m_PlaneNode3->SetProperty("name", mitk::StringProperty::New(std::string(renderer3->GetName()) + ".plane"));
//   m_PlaneNode3->SetProperty("includeInBoundingBox", mitk::BoolProperty::New(false));
//   m_PlaneNode3->SetProperty("helper object", mitk::BoolProperty::New(true));
////   mapper = mitk::PlaneGeometryDataMapper2D::New();
////   m_PlaneNode3->SetMapper(mitk::BaseRenderer::Standard2D, mapper);

   m_ParentNodeForGeometryPlanes = mitk::DataNode::New();
   m_ParentNodeForGeometryPlanes->SetProperty("name", mitk::StringProperty::New("Widgets"));
   m_ParentNodeForGeometryPlanes->SetProperty("helper object", mitk::BoolProperty::New(true));
 }

 void CustomisedWidget::AddPlanesToDataStorage()
 {
   if (m_PlaneNode1.IsNotNull() && m_PlaneNode2.IsNotNull() && m_PlaneNode3.IsNotNull() &&
       m_ParentNodeForGeometryPlanes.IsNotNull())
   {
     if (m_DataStorage.IsNotNull())
     {
       m_DataStorage->Add(m_ParentNodeForGeometryPlanes);
       m_DataStorage->Add(m_PlaneNode1, m_ParentNodeForGeometryPlanes);
       m_DataStorage->Add(m_PlaneNode2, m_ParentNodeForGeometryPlanes);
       m_DataStorage->Add(m_PlaneNode3, m_ParentNodeForGeometryPlanes);
     }
   }
 }


 void CustomisedWidget::SetWidgetPlaneVisibility(const char *widgetName, bool visible, mitk::BaseRenderer *renderer)
 {
   if (m_DataStorage.IsNotNull())
   {
     mitk::DataNode *n = m_DataStorage->GetNamedNode(widgetName);
     if (n != NULL)
       n->SetVisibility(visible, renderer);
   }
 }

 void CustomisedWidget::SetWidgetPlanesVisibility(bool visible, mitk::BaseRenderer *renderer)
 {
   if (m_PlaneNode1.IsNotNull())
   {
     m_PlaneNode1->SetVisibility(visible, renderer);
   }
   if (m_PlaneNode2.IsNotNull())
   {
     m_PlaneNode2->SetVisibility(visible, renderer);
   }
   if (m_PlaneNode3.IsNotNull())
   {
     m_PlaneNode3->SetVisibility(visible, renderer);
   }
   m_RenderingManager->RequestUpdateAll();
 }





 void CustomisedWidget::SetDataStorage(mitk::DataStorage *ds)
 {
   if (ds == m_DataStorage)
   {
     return;
   }

   mitk::BaseRenderer::GetInstance(mitkWidget1->GetRenderWindow())->SetDataStorage(ds);
   mitk::BaseRenderer::GetInstance(mitkWidget2->GetRenderWindow())->SetDataStorage(ds);
   mitk::BaseRenderer::GetInstance(mitkWidget3->GetRenderWindow())->SetDataStorage(ds);
   mitk::BaseRenderer::GetInstance(mitkWidget4->GetRenderWindow())->SetDataStorage(ds);
   m_DataStorage = ds;
 }




 void CustomisedWidget::EnableStandardLevelWindow()
 {
   levelWindowWidget->disconnect(this);
   levelWindowWidget->SetDataStorage(mitk::BaseRenderer::GetInstance(mitkWidget1->GetRenderWindow())->GetDataStorage());
   levelWindowWidget->show();
 }

 void CustomisedWidget::DisableStandardLevelWindow()
 {
   levelWindowWidget->disconnect(this);
   levelWindowWidget->hide();
 }


 void CustomisedWidget::ResetCrosshair()
 {
   if (m_DataStorage.IsNotNull())
   {
     m_RenderingManager->InitializeViewsByBoundingObjects(m_DataStorage);
     // m_RenderingManager->InitializeViews( m_DataStorage->ComputeVisibleBoundingGeometry3D() );
     // reset interactor to normal slicing
     this->SetWidgetPlaneMode(PLANE_MODE_SLICING);
   }
 }

 void CustomisedWidget::SetWidgetPlaneMode(int userMode)
 {
//   MITK_DEBUG << "Changing crosshair mode to " << userMode;

//   emit WidgetNotifyNewCrossHairMode(userMode);
   // Convert user interface mode to actual mode
   {
     switch (userMode)
     {
       case 0:
         m_MouseModeSwitcher->SetInteractionScheme(mitk::MouseModeSwitcher::InteractionScheme::MITK);
         break;
       case 1:
         m_MouseModeSwitcher->SetInteractionScheme(mitk::MouseModeSwitcher::InteractionScheme::ROTATION);
         break;

       case 2:
         m_MouseModeSwitcher->SetInteractionScheme(mitk::MouseModeSwitcher::InteractionScheme::ROTATIONLINKED);
         break;

       case 3:
         m_MouseModeSwitcher->SetInteractionScheme(mitk::MouseModeSwitcher::InteractionScheme::SWIVEL);
         break;
     }
   }
 }



 void CustomisedWidget::SetWidgetPlaneModeToSlicing(bool activate)
 {
   if (activate)
   {
     this->SetWidgetPlaneMode(PLANE_MODE_SLICING);
   }
 }

 void CustomisedWidget::SetWidgetPlaneModeToRotation(bool activate)
 {
   if (activate)
   {
     this->SetWidgetPlaneMode(PLANE_MODE_ROTATION);
   }
 }

 void CustomisedWidget::SetWidgetPlaneModeToSwivel(bool activate)
 {
   if (activate)
   {
     this->SetWidgetPlaneMode(PLANE_MODE_SWIVEL);
   }
 }

 void CustomisedWidget::getmouse(QMouseEvent *event)
 {

 }

 void CustomisedWidget::lang(QString lang)
 {
     qDebug()<<lang;
     if (lang == "en"){
     SetDecorationProperties(tr("Axial view").toStdString(), GetDecorationColor(0), 0);
     SetDecorationProperties(tr("Sagittal view").toStdString(), GetDecorationColor(1), 1);
     SetDecorationProperties(tr("Coronal view").toStdString(), GetDecorationColor(2), 2);
     SetDecorationProperties("3D view", GetDecorationColor(3), 3);
     }
     if (lang == "pl"){
     SetDecorationProperties(tr("Rzut pionowy").toStdString(), GetDecorationColor(0), 0);
     SetDecorationProperties(tr("Rzut boczny").toStdString(), GetDecorationColor(1), 1);
     SetDecorationProperties(tr("Rzut frontowy").toStdString(), GetDecorationColor(2), 2);
     SetDecorationProperties("Rzut 3D", GetDecorationColor(3), 3);
     }

 }

 void CustomisedWidget::brset(int brush,bool dual)
 {
     mitkWidget1->computing(brush,dual);
     mitkWidget2->computing(brush,dual);
     mitkWidget3->computing(brush,dual);
 }


 mitk::DataNode::Pointer CustomisedWidget::GetWidgetPlane1()
 {
   return this->m_PlaneNode1;
 }

 mitk::DataNode::Pointer CustomisedWidget::GetWidgetPlane2()
 {
   return this->m_PlaneNode2;
 }

 mitk::DataNode::Pointer CustomisedWidget::GetWidgetPlane3()
 {
   return this->m_PlaneNode3;
 }

 mitk::DataNode::Pointer CustomisedWidget::GetWidgetPlane(int id)
 {
   switch (id)
   {
     case 1:
       return this->m_PlaneNode1;
       break;
     case 2:
       return this->m_PlaneNode2;
       break;
     case 3:
       return this->m_PlaneNode3;
       break;
     default:
       return NULL;
   }
 }

