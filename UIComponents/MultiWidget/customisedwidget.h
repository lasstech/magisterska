#ifndef CUSTOMISEDWIDGET_H
#define CUSTOMISEDWIDGET_H
//#include <QmitkStdMultiWidget.h>
#include "customrenderwindow.h"


#include "MitkQtWidgetsExports.h"

#include <mitkDataStorage.h>
#include <mitkLogoAnnotation.h>

#include <mitkMouseModeSwitcher.h>

#include <QFrame>
#include <qsplitter.h>
#include <qwidget.h>

#include <QmitkLevelWindowWidget.h>
#include <QmitkRenderWindow.h>

#include <mitkBaseRenderer.h>
#include <mitkImage.h>

class CustomRenderWindow;
class vtkCornerAnnotation;
class vtkMitkRectangleProp;


namespace mitk
{
  class RenderingManager;
}


class MITKQTWIDGETS_EXPORT CustomisedWidget : public QWidget
{
    Q_OBJECT
public:
    CustomisedWidget(
            QWidget *parent = 0,
            Qt::WindowFlags f = 0,
            mitk::RenderingManager *renderingManager = 0,
            mitk::BaseRenderer::RenderingMode::Type renderingMode = mitk::BaseRenderer::RenderingMode::Standard,
            const QString &name = "stdmulti");
    virtual ~CustomisedWidget();
    CustomRenderWindow *mitkWidget1;
    CustomRenderWindow *mitkWidget2;
    CustomRenderWindow *mitkWidget3;
    CustomRenderWindow *mitkWidget4;
    QmitkLevelWindowWidget *levelWindowWidget;

    enum
    {
      AXIAL,
      SAGITTAL,
      CORONAL,
      THREE_D
    };


    enum
    {
      PLANE_MODE_SLICING = 0,
      PLANE_MODE_ROTATION,
      PLANE_MODE_SWIVEL
    };

    enum
    {
      MOUSE_MODE_SLICING = 0,
      MOUSE_MODE_ROTATION,
      MOUSE_MODE_ROTATION_LINKED,
      MOUSE_MODE_SWIVEL
    };

    /**
     * @brief SetCornerAnnotation Create a corner annotation for a widget.
     * @param text The text of the annotation.
     * @param color The color.
     * @param widgetNumber The widget (0-3).
     */
    void SetDecorationProperties(std::string text, mitk::Color color, int widgetNumber);

    void InitializeWidget();
    void InitialiseLabels();

    void FillGradientBackgroundWithBlack();

    mitk::Color GetDecorationColor(unsigned int widgetNumber);
    QmitkRenderWindow *GetRenderWindow1() const;
    QmitkRenderWindow *GetRenderWindow2() const;
    QmitkRenderWindow *GetRenderWindow3() const;
    QmitkRenderWindow *GetRenderWindow4() const;
    QmitkRenderWindow *GetRenderWindow(unsigned int number);
    void initImage(mitk::Image::Pointer pointer);
    void inline widgetIterate(mitk::Image::Pointer pointer, CustomRenderWindow &widget);
    void AddDisplayPlaneSubTree();
    void AddPlanesToDataStorage();
    void SetWidgetPlaneVisibility(const char *widgetName, bool visible, mitk::BaseRenderer *renderer = NULL);
    void SetWidgetPlanesVisibility(bool visible, mitk::BaseRenderer *renderer = NULL);
    void SetDataStorage(mitk::DataStorage *ds);
    void EnableStandardLevelWindow();
    void DisableStandardLevelWindow();
    mitk::DataNode::Pointer GetWidgetPlane1();
    mitk::DataNode::Pointer GetWidgetPlane2();
    mitk::DataNode::Pointer GetWidgetPlane3();
    mitk::DataNode::Pointer GetWidgetPlane(int id);
    void ResetCrosshair();
    mitk::MouseModeSwitcher::Pointer m_MouseModeSwitcher;

public slots:

    void SetWidgetPlaneMode(int mode);

    void SetWidgetPlaneModeToSlicing(bool activate);

    void SetWidgetPlaneModeToRotation(bool activate);

    void SetWidgetPlaneModeToSwivel(bool activate);

    void getmouse(QMouseEvent* event);

    void lang(QString lang);
    void brset(int brush, bool dual);

signals:
    void WidgetNotifyNewCrossHairMode(int);
    void wWasClicked(mitk::Point3D point);
    void wWasDraged(mitk::Point3D point);
    void wWasReleased();
    void paint(bool paint);
    void changePaint(int mode);
    void brushSize(int mode);
private:
    QHBoxLayout *QmitkStdMultiWidgetLayout;

    int m_Layout;
    int m_PlaneMode;

    mitk::RenderingManager *m_RenderingManager;

    mitk::LogoAnnotation::Pointer m_LogoRendering;

    bool m_GradientBackgroundFlag;

    mitk::SliceNavigationController *m_TimeNavigationController;

    mitk::DataStorage::Pointer m_DataStorage;

    mitk::DataNode::Pointer m_PlaneNode1;
    mitk::DataNode::Pointer m_PlaneNode2;
    mitk::DataNode::Pointer m_PlaneNode3;
    mitk::DataNode::Pointer m_ParentNodeForGeometryPlanes;
    mitk::Color m_DecorationColorWidget4;
    std::pair<mitk::Color, mitk::Color> m_GradientBackgroundColors[4];

    QSplitter *m_MainSplit;
    QSplitter *m_LayoutSplit;
    QSplitter *m_SubSplit1;
    QSplitter *m_SubSplit2;

    QWidget *mitkWidget1Container;
    QWidget *mitkWidget2Container;
    QWidget *mitkWidget3Container;
    QWidget *mitkWidget4Container;

    vtkSmartPointer<vtkCornerAnnotation> m_CornerAnnotations[4];
    vtkSmartPointer<vtkMitkRectangleProp> m_RectangleProps[4];

    bool m_PendingCrosshairPositionEvent;
    bool m_CrosshairNavigationEnabled;
    vtkSmartPointer<vtkCornerAnnotation> CreateCornerAnnotation(std::string text, mitk::Color color);

  };

#endif // CUSTOMISEDWIDGET_H
