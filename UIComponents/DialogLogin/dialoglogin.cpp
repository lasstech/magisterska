#include "dialoglogin.h"
#include "ui_dialoglogin.h"
#include "IO/SQL/enums.h"
#include <QDebug>

DialogLogin::DialogLogin(IOHandler *hand, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogLogin)
{
    ui->setupUi(this);
    handler=hand;
    QList<QStringList> list = handler->sql->SQLSelectUsers();
    for (int var = 0; var < list.length(); ++var) {
        ui->registered_logins->addItem(list[var][0],list[var][(int) SqlEnumUsers::Password]);
    }
    ui->line_password->setEchoMode(QLineEdit::Password);
    connect(ui->log_or_sign, SIGNAL(activated(int)),
            ui->stackedWidget, SLOT(setCurrentIndex(int)));
}

DialogLogin::~DialogLogin()
{
    delete ui;
}

bool DialogLogin::on_push_ok_clicked()
{
    if (ui->log_or_sign->currentIndex()==0){
        if (ui->line_password->text()==ui->registered_logins->currentData().toString()){
            QString user = ui->registered_logins->currentText();
            emit veryfied(user);
            return true;
        }
        else {
            return false;
        }
    } else if (ui->log_or_sign->currentIndex()==1){
        QString newy = handler->sql->SQLSelectUser(0,ui->new_login->text());
        if(newy!="error"){
            error(tr("login allready exists"));
        } else {
            handler->sql->SQLInsertuser(ui->new_login->text(),ui->new_password->text());
            emit veryfied(ui->new_login->text());
        }
    }
}

void DialogLogin::on_push_cancel_clicked()
{
    this->~DialogLogin();
    QCoreApplication::quit();
}


void DialogLogin::on_log_or_sign_activated(int index)
{
    if (index == 0){
        DialogLogin::setWindowTitle(tr("Log in window"));
    }
    if (index == 1){
        DialogLogin::setWindowTitle(tr("Sign in window"));
    }

}

void DialogLogin::error(QString error){
    qDebug()<<error;
}
