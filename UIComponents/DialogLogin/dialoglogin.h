#ifndef DIALOGLOGIN_H
#define DIALOGLOGIN_H

#include <QDialog>
#include "IO/Handler/iohandler.h"

namespace Ui {
class DialogLogin;
}

class DialogLogin : public QDialog
{
    Q_OBJECT

public:
    explicit DialogLogin(IOHandler *handler, QWidget *parent = 0);
    ~DialogLogin();
    void error(QString error);
public:
    IOHandler *handler;
signals:
    void veryfied(QString user);
private slots:
    bool on_push_ok_clicked();

    void on_push_cancel_clicked();
    void on_log_or_sign_activated(int index);

private:
    Ui::DialogLogin *ui;
};

#endif // DIALOGLOGIN_H
