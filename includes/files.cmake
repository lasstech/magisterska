
include_directories(IO
    RegionGrowing
    vectors
    translations
    UIComponents/Mainwindow
    Opening)

SET(TRANSLATIONS
    Translations/trans_pl.ts
    Translations/trans_en.ts
)


set(CPP_FILES
  main.cpp
  UIComponents/Mainwindow/mainwindow.cpp
  )



set (QT_LIBS
    Qt5::Core
    Qt5::Quick
    Qt5::Sql
    )

set(UI_FILES
    UIComponents/Mainwindow/mainwindow.ui
    UIComponents/DialogLogin/dialoglogin.ui
    )


qt5_wrap_ui(Cmake_form_hdr ${UI_FILES})
