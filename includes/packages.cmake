
find_package(MITK REQUIRED
)

find_package(ITK REQUIRED)
include(${ITK_USE_FILE})
find_package(Qt5Widgets)
find_package(Qt5 REQUIRED COMPONENTS Core Quick Sql)
find_package(Qt5LinguistTools)
