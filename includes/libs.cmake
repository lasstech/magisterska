




include_directories(    Algorythms/vectors)
add_library(            vector                     SHARED      Algorythms/vectors/vector.cpp)
target_link_libraries(  vector                                 MitkCore Qt5::Core)


include_directories(    Algorythms/Painter)
add_library(            Painter                     SHARED      Algorythms/Painter/painting.cpp)
target_link_libraries(  Painter                                 MitkCore Qt5::Core)

include_directories(    IO)
add_library(            IO                          SHARED      IO/Handler/iohandler.cpp IO/SQL/sql.cpp)
target_link_libraries(  IO                                      MitkCore Qt5::Core Qt5::Sql)

#include_directories(    UIComponents/Mainwindow)
#add_library(            MainWindow                  SHARED      UIComponents/Mainwindow/mainwindow.cpp
#    )
#target_link_libraries(  MainWindow                              MitkCore MitkQtWidgetsExt Qt5::Core Qt5::Sql Painter)

include_directories(    UIComponents/MultiWidget)
add_library(            Customisedwidget            SHARED      UIComponents/MultiWidget/customisedwidget.cpp)
target_link_libraries(  Customisedwidget                        MitkCore MitkQtWidgetsExt Qt5::Core Qt5::Sql)

include_directories(    UIComponents/RenderWindow)
add_library(            Customrenderwindow          SHARED      UIComponents/RenderWindow/customrenderwindow.cpp)
target_link_libraries(  Customrenderwindow                      MitkCore MitkQtWidgetsExt Qt5::Core Qt5::Sql Painter)

include_directories(    UIComponents/DialogLogin)
add_library(            Daloglogin                  SHARED      UIComponents/DialogLogin/dialoglogin.cpp)
target_link_libraries(  Daloglogin                              MitkCore MitkQtWidgetsExt Qt5::Core Qt5::Sql)

include_directories(    Glue)
add_library(            Glue                        SHARED      UIComponents/Glue/glue.cpp)
target_link_libraries(  Glue                                    MitkCore MitkQtWidgetsExt Qt5::Core Qt5::Sql Daloglogin)


include_directories(    Algorythms/Morphological/Opening
    ${Qt5Widgets_INCLUDE_DIRS})
add_library(            Opening                     SHARED      Algorythms/Morphological/Opening/Opening.txx
                                                                Algorythms/Morphological/Opening/Opening1.cpp
                                                                Algorythms/Morphological/Opening/Opening2.cpp
                                                                Algorythms/Morphological/Opening/Opening3.cpp)
target_link_libraries(  Opening                                 MitkCore Qt5::Core)

#include_directories(    Algorythms/Morphological/OpeningR
#    ${Qt5Widgets_INCLUDE_DIRS})
#add_library(            OpeningR                     SHARED      Algorythms/Morphological/OpeningR/OpeningR.txx
#                                                                Algorythms/Morphological/OpeningR/OpeningR1.cpp
#                                                                Algorythms/Morphological/OpeningR/OpeningR2.cpp
#                                                                Algorythms/Morphological/OpeningR/OpeningR3.cpp)
#target_link_libraries(  OpeningR                                MitkCore Qt5::Core)


include_directories(    Algorythms/RegionGrowing
    ${Qt5Widgets_INCLUDE_DIRS}
#    ${Qt5Sql_INCLUDE_DIRS}
    )
add_library(            RegionGrowing               SHARED     Algorythms/RegionGrowing/RegionGrowing.txx
                                                                Algorythms/RegionGrowing/RegionGrowing1.cpp
                                                                Algorythms/RegionGrowing/RegionGrowing2.cpp
                                                                Algorythms/RegionGrowing/RegionGrowing3.cpp)
target_link_libraries(  RegionGrowing                                MitkCore Qt5::Core IO)

include_directories(    Algorythms/Smoothing
    ${Qt5Widgets_INCLUDE_DIRS}
#    ${Qt5Sql_INCLUDE_DIRS}
    )
add_library(            Smoothing               SHARED     Algorythms/SmoothingFilter/Smoothing.txx
                                                                Algorythms/SmoothingFilter/Smoothing1.cpp
                                                                Algorythms/SmoothingFilter/Smoothing2.cpp
                                                                Algorythms/SmoothingFilter/Smoothing3.cpp)
target_link_libraries(  Smoothing                                MitkCore Qt5::Core IO)








set(Mylibs Painter
    IO
#    MainWindow
    Opening
#    OpeningR
    Smoothing
    RegionGrowing
    Customisedwidget
    Customrenderwindow
    Daloglogin
    Glue
    vector
    )
