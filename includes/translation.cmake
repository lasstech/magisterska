

qt5_create_translation(QM_FILES ${CPP_FILES} ${MOC_H_FILES} ${Cmake_form_hdr} ${TRANSLATIONS})



#target_link_libraries(IOConstr MitkCore MitkCommandLine ${ITK_LIBRARIES})
file (GLOB TRANSLATIONS_FILES Translations/*.ts)

option (UPDATE_TRANSLATIONS "Update source translation translations/*.ts")
if (UPDATE_TRANSLATIONS)
  qt5_create_translation(QM_FILES ${FILES_TO_TRANSLATE}
${TRANSLATIONS_FILES})
else (UPDATE_TRANSLATIONS)
  qt5_add_translation(QM_FILES ${TRANSLATIONS_FILES})
endif (UPDATE_TRANSLATIONS)

add_custom_target (translations_target DEPENDS ${QM_FILES})

install(FILES ${QM_FILES} DESTINATION ${CMAKE_INSTALL_PREFIX}/Translations)

# prevent the generated files from being deleted during make clean
set_directory_properties(PROPERTIES CLEAN_NO_CUSTOM true)

#file(MAKE_DIRECTORY Translations)
