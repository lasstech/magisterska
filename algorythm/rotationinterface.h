#ifndef ROTATIONINTERFACE_H
#define ROTATIONINTERFACE_H

#include <mitkVector.h>

class RotationInterface
{
public:
    RotationInterface();
    mitk::Vector3D transform(mitk::Vector3D vector, mitk::Vector3D v, double a);
};

#endif // ROTATIONINTERFACE_H
