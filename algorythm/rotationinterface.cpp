#include "rotationinterface.h"
#include <cmath>
#include <qdebug.h>
#include <cmath>

RotationInterface::RotationInterface()
{

}

mitk::Vector3D RotationInterface::transform(mitk::Vector3D vector, mitk::Vector3D v, double a){
    double mat[3][3];
    mitk::Vector3D turned;
    double nomaliser =0.0;
    double R[3][3] = {
        {cos(a)+pow(v[0],2)*(1-cos(a)),             (v[1]*v[0])*(1-cos(a)) -(v[2]*sin(a)),      (v[2]*v[0])*(1-cos(a)) +(v[1]*sin(a))},
        {(v[1]*v[0])*(1-cos(a)) +(v[2]*sin(a)),     cos(a)+pow(v[1],2)*(1-cos(a)),              (v[1]*v[2])*(1-cos(a)) -(v[0]*sin(a))},
        {(v[2]*v[0])*(1-cos(a)) -(v[1]*sin(a)),     (v[1]*v[2])*(1-cos(a)) +(v[0]*sin(a)),      cos(a)+pow(v[2],2)*(1-cos(a))}
    };

    for (int i = 0; i < 3; ++i) {
            turned[i] =(R[i][0]*vector[0])+(R[i][1]*vector[1])+(R[i][2]*vector[2]);
//            if (fabs(turned[i])<=0.001){
//                turned[i]=0.0;
//            }
            nomaliser+=fabs(turned[i]);
    }
    for (int i = 0; i < 3; ++i) {
            turned[i] =turned[i]/nomaliser;
    }

    return turned;
}

